object frmLevel: TfrmLevel
  Left = 210
  Top = 72
  Width = 1245
  Height = 833
  Caption = 'Levels'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 209
    Height = 797
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 174
      Top = 683
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 209
      Height = 665
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lposition: TLabel
        Left = 16
        Top = 256
        Width = 3
        Height = 13
      end
      object Label2: TLabel
        Left = 16
        Top = 496
        Width = 54
        Height = 13
        Caption = 'Viata Angry'
      end
      object l1: TSpeedButton
        Left = 8
        Top = 560
        Width = 23
        Height = 22
        Caption = '1'
        OnClick = l1Click
      end
      object l2: TSpeedButton
        Tag = 1
        Left = 32
        Top = 560
        Width = 23
        Height = 22
        Caption = '2'
        OnClick = l1Click
      end
      object l3: TSpeedButton
        Tag = 2
        Left = 56
        Top = 560
        Width = 23
        Height = 22
        Caption = '3'
        OnClick = l1Click
      end
      object l4: TSpeedButton
        Tag = 3
        Left = 80
        Top = 560
        Width = 23
        Height = 22
        Caption = '4'
        OnClick = l1Click
      end
      object l5: TSpeedButton
        Tag = 4
        Left = 104
        Top = 560
        Width = 23
        Height = 22
        Caption = '5'
        OnClick = l1Click
      end
      object l6: TSpeedButton
        Tag = 5
        Left = 128
        Top = 560
        Width = 23
        Height = 22
        Caption = '6'
        OnClick = l1Click
      end
      object l7: TSpeedButton
        Tag = 6
        Left = 152
        Top = 560
        Width = 25
        Height = 22
        Caption = '7'
        OnClick = l1Click
      end
      object Button2: TButton
        Left = 16
        Top = 432
        Width = 161
        Height = 25
        Caption = 'Delete'
        TabOrder = 0
        OnClick = Button2Click
      end
      object Button1: TButton
        Left = 96
        Top = 400
        Width = 81
        Height = 25
        Caption = 'Add'
        TabOrder = 1
        OnClick = Button1Click
      end
      object seLayersCount: TSpinEdit
        Left = 16
        Top = 400
        Width = 49
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 1
      end
      object cbLayers: TComboBox
        Left = 16
        Top = 368
        Width = 169
        Height = 21
        AutoDropDown = True
        AutoCloseUp = True
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
      end
      object cbChangeLayer: TComboBox
        Left = 16
        Top = 328
        Width = 169
        Height = 21
        AutoDropDown = True
        AutoCloseUp = True
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 4
        Text = 'Layer 1'
        OnChange = cbChangeLayerChange
        Items.Strings = (
          'Layer 1'
          'Layer 2'
          'Layer 3')
      end
      object pMainFrame: TPanel
        Left = 8
        Top = 64
        Width = 193
        Height = 185
        BevelOuter = bvNone
        TabOrder = 5
        object iMainFrame: TImage
          Left = 16
          Top = 16
          Width = 153
          Height = 161
          OnClick = iMainFrameClick
        end
      end
      object cbObjects: TComboBox
        Left = 24
        Top = 24
        Width = 145
        Height = 21
        AutoDropDown = True
        AutoCloseUp = True
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
        OnChange = cbObjectsChange
      end
      object CheckBox1: TCheckBox
        Left = 24
        Top = 288
        Width = 97
        Height = 17
        Caption = 'Invers'
        TabOrder = 7
        OnClick = CheckBox1Click
      end
      object CheckBox2: TCheckBox
        Left = 16
        Top = 464
        Width = 97
        Height = 17
        Caption = 'Ascunde Layer 1'
        TabOrder = 8
        OnClick = CheckBox2Click
      end
      object SpinEdit2: TSpinEdit
        Left = 88
        Top = 488
        Width = 73
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 9
        Value = 0
        OnChange = SpinEdit2Change
      end
      object Button4: TButton
        Left = 48
        Top = 520
        Width = 75
        Height = 25
        Caption = 'Seteaza'
        Enabled = False
        TabOrder = 10
        OnClick = Button4Click
      end
      object ComboBox1: TComboBox
        Left = 8
        Top = 600
        Width = 185
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 11
        OnChange = ComboBox1Change
        Items.Strings = (
          'Plaja'
          'Munte '
          'Deal')
      end
      object Button5: TButton
        Left = 16
        Top = 632
        Width = 97
        Height = 25
        Caption = 'Save'
        Enabled = False
        TabOrder = 12
        OnClick = Button5Click
      end
      object Button6: TButton
        Left = 120
        Top = 632
        Width = 75
        Height = 25
        Caption = 'Missions'
        TabOrder = 13
        OnClick = Button6Click
      end
    end
    object SpinEdit1: TSpinEdit
      Left = 120
      Top = 680
      Width = 49
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 100
      OnChange = SpinEdit1Change
    end
    object Button3: TButton
      Left = 16
      Top = 678
      Width = 97
      Height = 25
      Caption = 'Goleste nivel'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button7: TButton
      Left = 72
      Top = 720
      Width = 75
      Height = 25
      Caption = 'Message'
      TabOrder = 3
      OnClick = Button7Click
    end
  end
  object ScrollBox1: TScrollBox
    Left = 209
    Top = 0
    Width = 1020
    Height = 797
    VertScrollBar.Position = 2222
    VertScrollBar.Tracking = True
    Align = alClient
    TabOrder = 1
    object Panel2: TPanel
      Left = 0
      Top = -2222
      Width = 32767
      Height = 3000
      BevelOuter = bvNone
      TabOrder = 0
      OnClick = Panel2Click
      OnMouseMove = Panel2MouseMove
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 489
    Top = 248
    object Delete1: TMenuItem
      Caption = 'Delete'
      OnClick = Delete1Click
    end
    object SentToBack1: TMenuItem
      Caption = 'Sent To Back'
      OnClick = SentToBack1Click
    end
    object BringtoFront1: TMenuItem
      Caption = 'Bring to Front'
      OnClick = BringtoFront1Click
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 329
    Top = 56
  end
  object PopupMenu2: TPopupMenu
    Left = 985
    Top = 128
    object Edit1: TMenuItem
      Caption = 'Edit'
      OnClick = Edit1Click
    end
    object Delete2: TMenuItem
      Caption = 'Delete'
      OnClick = Delete2Click
    end
  end
end
