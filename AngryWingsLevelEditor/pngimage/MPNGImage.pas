unit MPNGImage;

//===============================About Program==================================

{
---------------------- Multilayered PNG Array Image Format ---------------------
-------------------------------- August 2008 -----------------------------------
--------------------------- By Mohammad Yousefi --------------------------------
------------------------- vahid_you2004@yahoo.com ------------------------------
--------------------------- Copyright 2008 MCoP --------------------------------
------------------------ http://mcop.netfirms.com/ -----------------------------
}

//==============================About Animation=================================

{
Each line of Animation property is a frame.

Line String Format: Interval,Task,Layer0Alpha,Layer1Alpha,...

Task:  n: Next Frame
       p: Pause
       s: Stop
       r: Reverse
       f: Forward
       i: Invert Direction
      rp: Reverse and Pause
      rs: Reverse and Stop
      fp: Forward and Pause
      fs: Forward and Stop
      ip: Invert Direction and Pause
      is: Invert Direction and Stop
}

//==========================Unit Interface======================================

interface

uses
  Graphics, SysUtils, Classes, Windows, pngimage, ExtCtrls, Clipbrd;

type

  EMPNGError = class(Exception);

  TMPNGImage = class;

  PMPNGLayerRec = ^TMPNGLayerRec;
  TMPNGLayerRec = record
    Image:TPNGObject;
    Visibility:Boolean;
    Alpha:Byte;
    Extra:TMemoryStream;
    Name:String[15];
  end;

  TMPNGLayers = class(TObject)
  private
    List:TList;
    Image:TMPNGImage;
    function GetCount:Integer;
    function GetItems(Index:Integer):TPngObject;
    procedure SetItems(Index:Integer;Value:TPngObject);
    function GetItemsV(Index:Integer):Boolean;
    procedure SetItemsV(Index:Integer;Value:Boolean);
    function GetItemsA(Index:Integer):Byte;
    procedure SetItemsA(Index:Integer;Value:Byte);
    function GetItemsN(Index:Integer):String;
    procedure SetItemsN(Index:Integer;Value:String);
    function GetItemsH(Index:Integer):Boolean;
  public
    constructor Create(AOwner:TMPNGImage);
    destructor Destroy; override;
    function Add:Integer;
    procedure Clear;
    procedure Delete(Index:Integer);
    procedure Insert(Index:Integer);
    procedure Move(CurIndex,NewIndex:Integer);
    function ReadExtraInfo(Index:Integer;Stream:TStream):Int64;
    procedure WriteExtraInfo(Index:Integer;Stream:TStream;Size:Int64);
    procedure DeleteExtraInfo(Index:Integer);

    property Count:Integer read GetCount;
    property Items[Index:Integer]:TPngObject read GetItems write SetItems;
    property ItemsVisibility[Index:Integer]:Boolean read GetItemsV write SetItemsV;
    property ItemsAlpha[Index:Integer]:Byte read GetItemsA write SetItemsA;
    property ItemsName[Index:Integer]:String read GetItemsN write SetItemsN;
    property ItemHasExtra[Index:Integer]:Boolean read GetItemsH;
  end;

  TMPNGImageHeader = packed record
    Name:String[15];
    Alpha:Byte;
    Visibility:Boolean;
    Offset,Size,ExtraInfoOffset,ExtraInfoSize:Cardinal;
  end;

  TImages = array [0..0] of TMPNGImageHeader;
  PImages = ^TImages;

  TMPNGImage = class(TGraphic)
  private
    Layers:TMPNGLayers;
    Img:TPngObject;
    fWidth,fHeight:Integer;
    fComment:String;
    fAnimation:TStrings;
    fAnimationRate:Real;
    fOnPlay,fOnPause,fOnStop,fOnFrame:TNotifyEvent;
    AniTim:TTimer;
    fAnimationFrame,NextFrame:Cardinal;
    fAnimationReverse:Boolean;

    function GetLayerCount:Integer;
    function GetLayerImage(Index:Integer):TPngObject;
    procedure SetLayerImage(Index:Integer;Value:TPngObject);
    function GetLayerV(Index:Integer):Boolean;
    procedure SetLayerV(Index:Integer;Value:Boolean);
    function GetLayerA(Index:Integer):Byte;
    procedure SetLayerA(Index:Integer;Value:Byte);
    procedure RefreshGraphic;
    procedure SetAnimation(Value:TStrings);
    procedure AniTimTimer(Sender:TObject);
    procedure SetAnimationFrame(Value:Cardinal);
    procedure ResizePNG(P:TPngObject);
    function GetLayerN(Index:Integer):String;
    procedure SetLayerN(Index:Integer;Value:String);
    function AddLayerWithoutRefresh(Image:TPngObject;Name:String):Integer;
    function GetLayerH(Index:Integer):Boolean;
  protected
    function GetEmpty:Boolean; override;
    function GetHeight:Integer; override;
    function GetWidth:Integer; override;
    procedure ReadData(Stream:TStream); override;
    procedure SetHeight(Value:Integer); override;
    procedure SetWidth(Value:Integer); override;
    procedure WriteData(Stream:TStream); override;
  public
    ExtraSignature:array [0..9] of Char;

    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source:TPersistent); override;
    procedure AssignTo(Dest:TPersistent); override;
    procedure Draw(ACanvas:TCanvas;const Rect:TRect); override;
    procedure LoadFromStream(Stream:TStream); override;
    procedure LoadFromResourceName(Instance:THandle;const ResName:String);
    procedure SaveToStream(Stream: TStream); override;
    procedure LoadFromClipboardFormat(AFormat:Word;AData:THandle;APalette:HPALETTE); override;
    procedure SaveToClipboardFormat(var AFormat:Word;var AData:THandle;var APalette:HPALETTE); override;
    {$IFDEF MSWINDOWS}
    procedure LoadFromResourceID(Instance:THandle;ResID:Integer);
    {$ENDIF}
    function GetLayerExtraInfo(Index:Integer;Stream:TStream):Int64;
    procedure SetLayerExtraInfo(Index:Integer;Stream:TStream;Size:Int64);
    function AddLayer(Image:TPngObject;Name:String):Integer;
    procedure InsertLayer(Index:Integer;Image:TPngObject;Name:String);
    procedure DeleteLayer(Index:Integer);
    procedure ClearLayers;
    procedure MoveLayer(CurIndex,NewIndex:Integer);
    procedure AnimationPlay;
    procedure AnimationPause;
    procedure AnimationStop;
    procedure CopyToClipboard(OnlyMPNG:Boolean=False);
    procedure PasteFromClipboard;
    procedure Reset;
    procedure ClearLayerExtraInfo(Index:Integer);

    property LayerCount:Integer read GetLayerCount;
    property LayerImage[Index:Integer]:TPngObject read GetLayerImage write SetLayerImage;
    property LayerVisibility[Index:Integer]:Boolean read GetLayerV write SetLayerV;
    property LayerAlpha[Index:Integer]:Byte read GetLayerA write SetLayerA;
    property LayerName[Index:Integer]:String read GetLayerN write SetLayerN;
    property LayerHasExtraInfo[Index:Integer]:Boolean read GetLayerH;
    property Comment:String read fComment write fComment;
    property Animation:TStrings read fAnimation write SetAnimation;
    property AnimationRate:Real read fAnimationRate write fAnimationRate;
    property AnimationCurrentFrame:Cardinal read fAnimationFrame write SetAnimationFrame;
    property AnimationReverse:Boolean read fAnimationReverse write fAnimationReverse;

    property OnPlay:TNotifyEvent read fOnPlay write fOnPlay;
    property OnPause:TNotifyEvent read fOnPause write fOnPause;
    property OnStop:TNotifyEvent read fOnStop write fOnStop;
    property OnFrame:TNotifyEvent read fOnFrame write fOnFrame;
  end;

var
  CF_MPNG : WORD;

  function GraphicToPNG(Graphic:TGraphic):TPngObject;
  procedure DrawPNGOnPNG(Src:TPngObject;Dest:TPngObject;Alpha:Byte);

implementation

//==============================General Methodes================================

function GraphicToPNG(Graphic:TGraphic):TPngObject;
var
  r,b:Graphics.TBitmap;
  x,y:Integer;
  t,n,l:PByteArray;
  c:TColor;
  a:Byte;

  procedure FC(RR,RG,RB,BR,BG,BB:Byte);
  var r,g,b:Byte;
  begin
  if RGB(RR,RG,RB)=RGB(BR,BG,BB) then
    begin
    a:=255;
    r:=RR;
    g:=RG;
    b:=RB;
    end
  else if (RR=255) and (BB=255) and (RG+RB+BR+BG=0) then
    begin
    a:=0;
    r:=0;
    g:=0;
    b:=0;
    end
  else
    begin
    a:=BR-RR+255;
    r:=BR*255 div a;
    g:=BG*255 div a;
    b:=RB*255 div a;
    end;
  c:=RGB(r,g,b);
  end;

begin
r:=Graphics.TBitmap.Create;
r.Canvas.Pen.Color:=clRed;
r.Canvas.Brush.Color:=clRed;
r.Width:=Graphic.Width;
r.Height:=Graphic.Height;
r.Canvas.FillRect(r.Canvas.ClipRect);
r.Canvas.Draw(0,0,Graphic);
r.PixelFormat:=pf24bit;
b:=Graphics.TBitmap.Create;
b.Canvas.Pen.Color:=clBlue;
b.Canvas.Brush.Color:=clBlue;
b.Width:=Graphic.Width;
b.Height:=Graphic.Height;
b.Canvas.FillRect(r.Canvas.ClipRect);
b.Canvas.Draw(0,0,Graphic);
b.PixelFormat:=pf24bit;
Result:=TPngObject.CreateBlank(COLOR_RGBALPHA,8,Graphic.Width,Graphic.Height);
for y:=Graphic.Height-1 downto 0 do
  begin
  t:=r.ScanLine[y];
  n:=b.ScanLine[y];
  l:=Result.AlphaScanline[y];
  for x:=0 to Graphic.Width-1 do
    begin
    FC(t^[x*3+2],t^[x*3+1],t^[x*3],n^[x*3+2],n^[x*3+1],n^[x*3]);
    Result.Pixels[x,y]:=c;
    l^[x]:=a;
    end;
  end;
r.Free;
b.Free;
end;

procedure DrawPNGOnPNG(Src:TPngObject;Dest:TPngObject;Alpha:Byte);
var
  f,t:PByteArray;
  x,y,m,h:Integer;
  c:TColor;
  a:Real;
  r,g,b,p,o,i,u:Byte;
  q,w,e,z:Integer;

  function ToByte(A:Real):Byte;
  begin
  if A<=0 then Result:=0
  else if A>=255 then Result:=255
  else Result:=Round(A);
  end;

begin
if Alpha=0 then Exit;
m:=Src.Width;
if Dest.Width<m then m:=Dest.Width;
h:=Src.Height;
if Dest.Height<h then h:=Dest.Height;
for y:=0 to h-1 do
  begin
  f:=Src.AlphaScanline[y];
  t:=Dest.AlphaScanline[y];
  for x:=0 to m-1 do
    begin
    a:=f^[x]*Alpha/255;
    if a=0 then Continue;
    c:=Src.Pixels[x,y];
    r:=GetRValue(c);
    g:=GetGValue(c);
    b:=GetBValue(c);
    u:=t^[x];
    c:=Dest.Pixels[x,y];
    p:=GetRValue(c);
    o:=GetGValue(c);
    i:=GetBValue(c);
    z:=Round(a+u-a*u/255);
    if z<>0 then
      begin
      q:=Round((r*a+p*u-(a*u*p/255))/z);
      w:=Round((g*a+o*u-(a*u*o/255))/z);
      e:=Round((b*a+i*u-(a*u*i/255))/z);
      Dest.Pixels[x,y]:=RGB(ToByte(q),ToByte(w),ToByte(e));
      t^[x]:=z;
      end;
    end;
  end;
end;

//================================TMPNGLayers===================================

constructor TMPNGLayers.Create(AOwner:TMPNGImage);
begin
List:=TList.Create;
Image:=AOwner;
end;

destructor TMPNGLayers.Destroy;
begin
Clear;
List.Free;
end;

function TMPNGLayers.Add:Integer;
var r:PMPNGLayerRec;
begin
r:=AllocMem(SizeOf(TMPNGLayerRec));
r^.Image:=TPngObject.CreateBlank(COLOR_RGBALPHA,8,Image.Width,Image.Height);
r^.Visibility:=True;
r^.Alpha:=255;
r^.Extra:=TMemoryStream.Create;
r^.Extra.Clear;
Result:=List.Add(r);
end;

procedure TMPNGLayers.Clear;
var k:Integer;
begin
for k:=0 to List.Count-1 do
  begin
  PMPNGLayerRec(List.Items[k])^.Image.Free;
  PMPNGLayerRec(List.Items[k])^.Extra.Free;
  FreeMem(List.Items[k]);
  end;
List.Clear;
end;

procedure TMPNGLayers.Delete(Index:Integer);
begin
PMPNGLayerRec(List.Items[Index])^.Image.Free;
PMPNGLayerRec(List.Items[Index])^.Extra.Free;
FreeMem(List.Items[Index]);
List.Delete(Index);
end;

procedure TMPNGLayers.Insert(Index:Integer);
var r:PMPNGLayerRec;
begin
r:=AllocMem(SizeOf(TMPNGLayerRec));
r^.Image:=TPngObject.CreateBlank(COLOR_RGBALPHA,8,Image.Width,Image.Height);
r^.Visibility:=True;
r^.Alpha:=255;
r^.Extra:=TMemoryStream.Create;
r^.Extra.Clear;
List.Insert(Index,r);
end;

procedure TMPNGLayers.Move(CurIndex,NewIndex:Integer);
begin
List.Move(CurIndex,NewIndex);
end;

function TMPNGLayers.GetCount:Integer;
begin
Result:=List.Count;
end;

function TMPNGLayers.GetItems(Index:Integer):TPngObject;
begin
Result:=PMPNGLayerRec(List.Items[Index]).Image;
end;

procedure TMPNGLayers.SetItems(Index:Integer;Value:TPngObject);
begin
PMPNGLayerRec(List.Items[Index])^.Image.Assign(Value);
end;

function TMPNGLayers.GetItemsV(Index:Integer):Boolean;
begin
Result:=PMPNGLayerRec(List.Items[Index])^.Visibility;
end;

procedure TMPNGLayers.SetItemsV(Index:Integer;Value:Boolean);
begin
PMPNGLayerRec(List.Items[Index])^.Visibility:=Value;
end;

function TMPNGLayers.GetItemsA(Index:Integer):Byte;
begin
Result:=PMPNGLayerRec(List.Items[Index])^.Alpha;
end;

procedure TMPNGLayers.SetItemsA(Index:Integer;Value:Byte);
begin
PMPNGLayerRec(List.Items[Index])^.Alpha:=Value;
end;

function TMPNGLayers.GetItemsN(Index:Integer):String;
begin
Result:=PMPNGLayerRec(List.Items[Index])^.Name;
end;

procedure TMPNGLayers.SetItemsN(Index:Integer;Value:String);
begin
PMPNGLayerRec(List.Items[Index])^.Name:=Value;
end;

function TMPNGLayers.ReadExtraInfo(Index:Integer;Stream:TStream):Int64;
begin
PMPNGLayerRec(List.Items[Index])^.Extra.Seek(0,soFromBeginning);
if GetItemsH(Index) then
  Result:=Stream.CopyFrom(PMPNGLayerRec(List.Items[Index])^.Extra,PMPNGLayerRec(List.Items[Index])^.Extra.Size)
else
  Result:=0;
end;

procedure TMPNGLayers.WriteExtraInfo(Index:Integer;Stream:TStream;Size:Int64);
begin
PMPNGLayerRec(List.Items[Index])^.Extra.Clear;
if Size>0 then
  PMPNGLayerRec(List.Items[Index])^.Extra.CopyFrom(Stream,Size);
end;

function TMPNGLayers.GetItemsH(Index:Integer):Boolean;
begin
Result:=PMPNGLayerRec(List.Items[Index])^.Extra.Size>0;
end;

procedure TMPNGLayers.DeleteExtraInfo(Index:Integer);
begin
PMPNGLayerRec(List.Items[Index])^.Extra.Clear;
end;

//==============================TMPNGImage======================================

constructor TMPNGImage.Create;
begin
inherited Create;
Layers:=TMPNGLayers.Create(Self);
fWidth:=0;
fHeight:=0;
fComment:='Created with MCoP MPNG Encoder v1.0';
Img:=TPngObject.CreateBlank(COLOR_RGBALPHA,8,fWidth,fHeight);
fAnimation:=TStringList.Create;
fAnimationRate:=1;
fAnimationFrame:=0;
AniTim:=TTimer.Create(nil);
AniTim.Enabled:=False;
AniTim.OnTimer:=AniTimTimer;
fAnimationReverse:=False;
fOnPlay:=nil;
fOnPause:=nil;
fOnStop:=nil;
fOnFrame:=nil;
FillChar(ExtraSignature,10,#0);
end;

destructor TMPNGImage.Destroy;
begin
Img.Free;
Layers.Destroy;
AniTim.Free;
fAnimation.Free;
inherited Destroy;
end;

procedure TMPNGImage.Assign(Source:TPersistent);
var
  p:TPngObject;
  k:Integer;
  m:TMemoryStream;
begin
if Source=Self then Exit;
Reset;
if Source is TMPNGImage then
  begin
  Animation:=(Source as TMPNGImage).Animation;
  fAnimationRate:=(Source as TMPNGImage).AnimationRate;
  fAnimationFrame:=(Source as TMPNGImage).AnimationCurrentFrame;
  fAnimationReverse:=(Source as TMPNGImage).AnimationReverse;
  fWidth:=(Source as TMPNGImage).Width;
  fHeight:=(Source as TMPNGImage).Height;
  fComment:=(Source as TMPNGImage).Comment;
  CopyMemory(@ExtraSignature,@(Source as TMPNGImage).ExtraSignature,10);
  m:=TMemoryStream.Create;
  for k:=0 to (Source as TMPNGImage).LayerCount-1 do
    begin
    AddLayerWithoutRefresh((Source as TMPNGImage).LayerImage[k],(Source as TMPNGImage).LayerName[k]);
    Layers.ItemsVisibility[k]:=(Source as TMPNGImage).LayerVisibility[k];
    Layers.ItemsAlpha[k]:=(Source as TMPNGImage).LayerAlpha[k];
    m.Clear;
    (Source as TMPNGImage).GetLayerExtraInfo(k,m);
    m.Seek(0,soFromBeginning);
    SetLayerExtraInfo(k,m,m.Size);
    end;
  m.Free;
  RefreshGraphic;
  inherited Changed(Self);
  end
else if Source is TPNGObject then
  begin
  AddLayerWithoutRefresh(Source as TPNGObject,'Layer 0');
  RefreshGraphic;
  inherited Changed(Self);
  end
else if Source is TGraphic then
  begin
  p:=GraphicToPNG(Source as TGraphic);
  AddLayerWithoutRefresh(p,'Layer 0');
  p.Free;
  RefreshGraphic;
  inherited Changed(Self);
  end
else if Source=Clipboard then
  PasteFromClipboard
else
  inherited Assign(Source);
end;

procedure TMPNGImage.Draw(ACanvas:TCanvas;const Rect:TRect);
var b:Graphics.TBitmap;
begin
if (Img.Width<>0) and (Img.Height<>0) then
  Img.Draw(ACanvas,Rect)
else
  begin
  b:=Graphics.TBitmap.Create;
  ACanvas.Draw(0,0,b);
  b.Free;
  end;
end;

function TMPNGImage.GetEmpty;
var k:Integer;
begin
Result:=(fWidth=0) or (fHeight=0) or (Layers.Count=0);
if not Result then Exit;
for k:=0 to Layers.Count-1 do
  if not Layers.Items[k].Empty then
    Exit;
Result:=True;
end;

function TMPNGImage.GetHeight:Integer;
begin
Result:=fHeight;
end;

function TMPNGImage.GetWidth: Integer;
begin
Result:=fWidth;
end;

function TMPNGImage.GetLayerCount:Integer;
begin
Result:=Layers.Count;
end;

function TMPNGImage.GetLayerImage(Index:Integer):TPngObject;
begin
Result:=Layers.Items[Index];
end;

procedure TMPNGImage.ResizePNG(P:TPngObject);
var
  x,y,w,h:Integer;
  b:PByteArray;
begin
w:=P.Width;
h:=P.Height;
P.Resize(fWidth,fHeight);
if (fWidth>w) or (fHeight>h) then
  for y:=0 to fHeight-1 do
    begin
    b:=P.AlphaScanline[y];
    for x:=0 to fWidth-1 do
      if (x>=w) or (y>=h) then
        b^[x]:=0;
    end;
end;

procedure TMPNGImage.SetLayerImage(Index:Integer;Value:TPngObject);
begin
Layers.Items[Index]:=Value;
if (fWidth=0) or (fHeight=0) then
  begin
  fWidth:=Value.Width;
  fHeight:=Value.Height;
  end;
if (Value.Width<>fWidth) or (Value.Height<>fHeight) then
  ResizePNG(Layers.Items[Index]);
RefreshGraphic;
inherited Changed(Self);
end;

function TMPNGImage.GetLayerV(Index:Integer):Boolean;
begin
Result:=Layers.ItemsVisibility[Index];
end;

procedure TMPNGImage.SetLayerV(Index:Integer;Value:Boolean);
begin
Layers.ItemsVisibility[Index]:=Value;
RefreshGraphic;
inherited Changed(Self);
end;

function TMPNGImage.GetLayerH(Index:Integer):Boolean;
begin
Result:=Layers.ItemHasExtra[Index];
end;

function TMPNGImage.GetLayerA(Index:Integer):Byte;
begin
Result:=Layers.ItemsAlpha[Index];
end;

procedure TMPNGImage.SetLayerA(Index:Integer;Value:Byte);
begin
Layers.ItemsAlpha[Index]:=Value;
RefreshGraphic;
inherited Changed(Self);
end;

function TMPNGImage.GetLayerN(Index:Integer):String;
begin
Result:=Layers.ItemsName[Index];
end;

procedure TMPNGImage.SetLayerN(Index:Integer;Value:String);
begin
if Length(Value)>15 then Value:=Copy(Value,1,15);
Layers.ItemsName[Index]:=Value;
inherited Changed(Self);
end;

procedure TMPNGImage.LoadFromResourceName(Instance:THandle;const ResName:string);
var s:TCustomMemoryStream;
begin
s:=TResourceStream.Create(Instance,ResName,'MPNG');
try
  LoadFromStream(s);
finally
  s.Free;
end;
end;

{$IFDEF MSWINDOWS}
procedure TMPNGImage.LoadFromResourceID(Instance:THandle;ResID:Integer);
var s:TCustomMemoryStream;
begin
s:=TResourceStream.CreateFromID(Instance,ResID,'MPNG');
try
  LoadFromStream(s);
finally
  s.Free;
end;
end;
{$ENDIF}

function TMPNGImage.GetLayerExtraInfo(Index:Integer;Stream:TStream):Int64;
begin
Result:=Layers.ReadExtraInfo(Index,Stream);
end;

procedure TMPNGImage.SetLayerExtraInfo(Index:Integer;Stream:TStream;Size:Int64);
begin
Layers.WriteExtraInfo(Index,Stream,Size);
inherited Changed(Self);
end;

procedure TMPNGImage.ReadData(Stream:TStream);
begin
LoadFromStream(Stream);
end;

procedure TMPNGImage.SetHeight(Value:Integer);
var k:Integer;
begin
if fHeight=Value then Exit;
fHeight:=Value;
for k:=0 to Layers.Count-1 do
  ResizePNG(Layers.Items[k]);
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.SetWidth(Value:Integer);
var k:Integer;
begin
if fWidth=Value then Exit;
fWidth:=Value;
for k:=0 to Layers.Count-1 do
  ResizePNG(Layers.Items[k]);
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.WriteData(Stream:TStream);
begin
SaveToStream(Stream);
end;

procedure TMPNGImage.RefreshGraphic;
var
  k:Integer;
  p:TPngObject;
begin
p:=TPngObject.CreateBlank(COLOR_RGBALPHA,8,fWidth,fHeight);
Img.Assign(p);
for k:=0 to Layers.Count-1 do
  if (Layers.ItemsVisibility[k]) and (Layers.ItemsAlpha[k]<>0) then
    begin
    p.Assign(Layers.Items[k]);
    p.CreateAlpha;
    DrawPNGOnPNG(p,Img,Layers.ItemsAlpha[k]);
    end;
p.Free;
end;

procedure TMPNGImage.LoadFromStream(Stream:TStream);
var
  b:array [0..9] of Char;
  r:Trect;
  c,o:Cardinal;
  k:Integer;
  h:PImages;
begin
Reset;
Progress(Self,psStarting,0,False,Rect(0,0,0,0),'Loading');
o:=Stream.Position;

//File Signature
Stream.Read(b,10);
if b<>'ARRAYOFPNG' then raise EMPNGError.Create('Can not read data or data is not MPNG');

//File Version
Stream.Read(c,4);
if c>0 then raise EMPNGError.Create('MPNG file version is higher than this encoder');

//Extra Signature
Stream.Read(ExtraSignature,10);

//File Comment
Stream.Read(c,4); //Size
fComment:='';
for k:=1 to c do
  begin
  Stream.Read(b,1);
  fComment:=fComment+b[0];
  end;

//Image Dimensions
Stream.Read(c,4);
fWidth:=c;
Stream.Read(c,4);
fHeight:=c;
r:=Rect(0,0,fWidth,fHeight);

//Number of Layers
Stream.Read(c,4);

//Images Headers
h:=AllocMem(SizeOf(TMPNGImageHeader)*c);
Stream.Read(h^,SizeOf(TMPNGImageHeader)*c);

//Images
for k:=0 to c-1 do
  begin
  if Layers.Add=-1 then
    begin
    FreeMem(h);
    raise EMPNGError.Create('Can not add layers to the object');
    end;
  Layers.ItemsName[k]:=h^[k].Name;
  Layers.ItemsAlpha[k]:=h^[k].Alpha;
  Layers.ItemsVisibility[k]:=h^[k].Visibility;
  Stream.Seek(h^[k].Offset+o,soFromBeginning);
  Layers.Items[k].LoadFromStream(Stream);
  Stream.Seek(h^[k].ExtraInfoOffset+o,soFromBeginning);
  Layers.WriteExtraInfo(k,Stream,h^[k].ExtraInfoSize);
  Progress(Self,psRunning,Round((k+1)*100/c),False,r,'Loading');
  end;

RefreshGraphic;
Progress(Self,psEnding,0,True,r,'Loading');
inherited Changed(Self);
end;

procedure TMPNGImage.SaveToStream(Stream:TStream);
var
  b:array [0..9] of Char;
  r:Trect;
  c,o,p:Cardinal;
  k:Integer;
  h:PImages;
begin
r:=Rect(0,0,fWidth,fHeight);
Progress(Self,psStarting,0,True,r,'Saving');
o:=Stream.Position;

//File Signature
b:='ARRAYOFPNG';
Stream.Write(b,10);

//File Version
c:=0;
Stream.Write(c,4);

//Extra Signature
Stream.Write(ExtraSignature,10);

//File Comment
c:=Length(fComment);
Stream.Write(c,4); //Size
for k:=1 to c do
  begin
  b[0]:=fComment[k];
  Stream.Write(b,1);
  end;

//Image Dimensions
c:=fWidth;
Stream.Write(c,4);
c:=fHeight;
Stream.Write(c,4);

//Number of Layers
c:=Layers.Count;
Stream.Write(c,4);

//Images Headers
h:=AllocMem(SizeOf(TMPNGImageHeader)*c);
p:=Stream.Position;  FillMemory(h,SizeOf(TMPNGImageHeader)*c,0);
Stream.Write(h^,SizeOf(TMPNGImageHeader)*c);

//Images
for k:=0 to c-1 do
  begin
  h^[k].Name:=Layers.ItemsName[k];
  h^[k].Alpha:=Layers.ItemsAlpha[k];
  h^[k].Visibility:=Layers.ItemsVisibility[k];
  h^[k].Offset:=Stream.Position-o;
  Layers.Items[k].SaveToStream(Stream);
  h^[k].Size:=Stream.Position-h^[k].Offset;
  h^[k].ExtraInfoOffset:=Stream.Position-o;
  h^[k].ExtraInfoSize:=Layers.ReadExtraInfo(k,Stream);
  Progress(Self,psRunning,Round((k+1)*100/c),True,r,'Saving');
  end;

//Saving Images Headers
Stream.Seek(p,soFromBeginning);
Stream.Write(h^,SizeOf(TMPNGImageHeader)*c);

Progress(Self,psEnding,0,True,r,'Saving');
end;

procedure TMPNGImage.AssignTo(Dest:TPersistent);
var b:Graphics.TBitmap;
begin
if Dest is TMPNGImage then
  Dest.Assign(Self)
else if Dest is TPNGObject then
  Dest.Assign(Img)
else if Dest is Graphics.TBitmap then
  Img.AssignTo(Dest)
else if Dest is TGraphic then
  begin
  b:=Graphics.TBitmap.Create;
  Img.AssignTo(b);
  Dest.Assign(b);
  b.Free;
  end
else
  inherited AssignTo(Dest);
end;

procedure TMPNGImage.SaveToClipboardFormat(var AFormat:Word;var AData:THandle;var APalette:HPALETTE);
var
  s:TMemoryStream;
  b:Pointer;
  d:THandle;
begin
if Empty then Exit;
s:=TMemoryStream.Create;
try
  SaveToStream(s);
  s.Position:=0;
  {$WARNINGS OFF}
  d:=GlobalAlloc(HeapAllocFlags,s.Size);
  {$WARNINGS ON}
  try
    if d<>0 then
      begin
      b:=GlobalLock(d);
      try
        Move(s.Memory^,b^,s.Size);
        AFormat:=CF_MPNG;
        APalette:=0;
        AData:=d;
      finally
        GlobalUnlock(d);
      end;
      end;
  except
    GlobalFree(d);
    raise;
  end;
finally
  s.Free;
end;
end;

procedure TMPNGImage.LoadFromClipboardFormat(AFormat:Word;AData:THandle;APalette:HPALETTE);
var
  z:Longint;
  u:Pointer;
  s:TMemoryStream;
  b:Graphics.TBitmap;
begin
if (AData<>0) and (AFormat=CF_MPNG) then
  begin
  z:=GlobalSize(AData);
  u:=GlobalLock(AData);
  try
    s:=TMemoryStream.Create;
    try
      s.SetSize(z);
      Move(u^,s.Memory^,z);
      LoadFromStream(s);
    finally
      s.Free;
    end;
  finally
    GlobalUnlock(AData);
  end;
  end
else if (AData<>0) and (AFormat=CF_BITMAP) then
  begin
  b:=Graphics.TBitmap.Create;
  try
    b.LoadFromClipboardFormat(AFormat,AData,APalette);
    Assign(b);
  finally
    b.Free;
  end;
  end
else
  raise EMPNGError.Create('Unsupported clipboard format');
end;

function TMPNGImage.AddLayerWithoutRefresh(Image:TPngObject;Name:String):Integer;
begin
Result:=Layers.Add;
if Length(Name)>15 then Name:=Copy(Name,1,15);
Layers.ItemsName[Result]:=Name;
Layers.Items[Result]:=Image;
if (fWidth=0) or (fHeight=0) then
  begin
  fWidth:=Image.Width;
  fHeight:=Image.Height;
  end;
if (Image.Width<>fWidth) or (Image.Height<>fHeight) then
  ResizePNG(Layers.Items[Result]);
end;

function TMPNGImage.AddLayer(Image:TPngObject;Name:String):Integer;
begin
AnimationStop;
Result:=AddLayerWithoutRefresh(Image,Name);
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.InsertLayer(Index:Integer;Image:TPngObject;Name:String);
begin
AnimationStop;
Layers.Insert(Index);
SetLayerN(Index,Name);
SetLayerImage(Index,Image);
end;

procedure TMPNGImage.ClearLayerExtraInfo(Index:Integer);
begin
Layers.DeleteExtraInfo(Index);
inherited Changed(Self);
end;

procedure TMPNGImage.DeleteLayer(Index:Integer);
begin
AnimationStop;
Layers.Delete(Index);
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.ClearLayers;
begin
AnimationStop;
Layers.Clear;
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.MoveLayer(CurIndex,NewIndex:Integer);
begin
Layers.Move(CurIndex,NewIndex);
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.SetAnimation(Value:TStrings);
begin
AnimationStop;
fAnimation.Assign(Value);
end;

procedure TMPNGImage.AnimationPlay;
begin
if fAnimation.Count=0 then Exit;
SetAnimationFrame(fAnimationFrame);
AniTim.Enabled:=True;
if Assigned(fOnPlay) then fOnPlay(Self);
end;

procedure TMPNGImage.AnimationPause;
begin
if not AniTim.Enabled then Exit;
AniTim.Enabled:=False;
if Assigned(fOnPause) then fOnPause(Self);
end;

procedure TMPNGImage.AnimationStop;
begin
if not AniTim.Enabled then Exit;
AniTim.Enabled:=False;
SetAnimationFrame(0);
if Assigned(fOnStop) then fOnStop(Self);
end;

procedure TMPNGImage.AniTimTimer(Sender:TObject);
begin
SetAnimationFrame(NextFrame);
if Assigned(fOnFrame) then fOnFrame(Self);
end;

procedure TMPNGImage.SetAnimationFrame(Value:Cardinal);
var
  s:String;
  k:Integer;

  function GN:Cardinal;
  var i:Integer;
  begin
  i:=Pos(',',s);
  if i=0 then
    begin
    Result:=StrToIntDef(s,0);
    s:='';
    end
  else
    begin
    Result:=StrToIntDef(Copy(s,1,i-1),0);
    Delete(s,1,i);
    end;
  end;

begin
if (fAnimation.Count=0) and (AniTim.Enabled) then
  begin
  AniTim.Enabled:=False;
  if Assigned(fOnStop) then fOnStop(Self);
  Exit;
  end;
if fAnimation.Count=0 then Exit;
if Value>=Cardinal(fAnimation.Count) then Value:=fAnimation.Count-1;
fAnimationFrame:=Value;
s:=fAnimation.Strings[Value];
if fAnimationRate=0 then fAnimationRate:=1;
AniTim.Interval:=Round(GN/fAnimationRate);
case UpCase(s[1]) of
  'N':        //next
    begin
    if fAnimationReverse then NextFrame:=Value-1 else NextFrame:=Value+1;
    Delete(s,1,2);
    end;
  'P':        //pause
    begin
    AnimationPause;
    Delete(s,1,2);
    end;
  'S':        //stop
    begin
    AnimationStop;
    Exit;
    end;
  'R':        //reverse
    begin
    fAnimationReverse:=True;
    NextFrame:=Value-1;
    if s[2]=',' then Delete(s,1,2) else Delete(s,1,1);
    end;
  'F':        //forward
    begin
    fAnimationReverse:=False;
    NextFrame:=Value+1;
    if s[2]=',' then Delete(s,1,2) else Delete(s,1,1);
    end;
  'I':        //invert direction
    begin
    fAnimationReverse:=not fAnimationReverse;
    if fAnimationReverse then NextFrame:=Value-1 else NextFrame:=Value+1;
    if s[2]=',' then Delete(s,1,2) else Delete(s,1,1);
    end;
  else
    NextFrame:=GN;
end;
case UpCase(s[1]) of
  'P':        //... and pause
    begin
    AnimationPause;
    Delete(s,1,2);
    end;
  'S':        //... and stop
    begin
    AnimationStop;
    Exit;
    end;
end;
for k:=0 to Layers.Count-1 do
  Layers.ItemsAlpha[k]:=GN;
RefreshGraphic;
inherited Changed(Self);
end;

procedure TMPNGImage.CopyToClipboard(OnlyMPNG:Boolean=False);
var
  f:Word;
  d:THandle;
  p:HPalette;
  b:Graphics.TBitmap;
begin
Clipboard.Open;
try
  SaveToClipboardFormat(f,d,p);
  Clipboard.SetAsHandle(f,d);
  if not OnlyMPNG then
    begin
    p:=0;
    Img.SaveToClipboardFormat(f,d,p);
    Clipboard.SetAsHandle(f,d);
    if p<>0 then Clipboard.SetAsHandle(CF_PALETTE,p);
    b:=Graphics.TBitmap.Create;
    AssignTo(b);
    p:=0;
    b.SaveToClipboardFormat(f,d,p);
    Clipboard.SetAsHandle(f,d);
    if p<>0 then Clipboard.SetAsHandle(CF_PALETTE,p);
    b.Free;
    end;  
finally
  Clipboard.Close;
end;
end;

procedure TMPNGImage.PasteFromClipboard;
begin
if Clipboard.HasFormat(CF_MPNG) then
  LoadFromClipboardFormat(CF_MPNG,Clipboard.GetAsHandle(CF_MPNG),0)
else if Clipboard.HasFormat(CF_BITMAP) then
  LoadFromClipboardFormat(CF_BITMAP,Clipboard.GetAsHandle(CF_BITMAP),Clipboard.GetAsHandle(CF_PALETTE));
end;

procedure TMPNGImage.Reset;
begin
AnimationStop;
Layers.Clear;
fWidth:=0;
fHeight:=0;
fAnimationReverse:=False;
fAnimationFrame:=0;
fAnimationRate:=1;
fComment:='Created with MCoP MPNG Encoder v1.0';
FillChar(ExtraSignature,10,#0);
RefreshGraphic;
inherited Changed(Self);
end;

//==========================Format Registration=================================

initialization
  TPicture.RegisterFileFormat('mpng','Multilayered PNG Array',TMPNGImage);
  CF_MPNG:=RegisterClipboardFormat('Multilayered PNG Array');
  TPicture.RegisterClipboardFormat(CF_MPNG,TMPNGImage);

finalization
  TPicture.UnregisterGraphicClass(TMPNGImage);

end.
