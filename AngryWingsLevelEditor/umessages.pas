unit umessages;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, entities, SQLite3, SQLiteTable3, math;

type
  TfrmMessage = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    CheckBox1: TCheckBox;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    Button1: TButton;
    Button2: TButton;
    ListBox1: TListBox;
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure loadMessage;
  public
    { Public declarations }
    message_id : integer;
    msgTexts : array of TTextMessage;
    msgText :  TTextMessage;
    sldb: TSQLiteDatabase;
    procedure loadData;
    procedure resetMessage;
  end;

var
  frmMessage: TfrmMessage;

implementation

{$R *.dfm}

{ TfrmMessage }

{ TfrmMessage }

procedure TfrmMessage.loadData;
var i : integer;
    sltb: TSQLIteTable;
begin
try
 edit1.Text := '';
 CheckBox1.Checked := true;
 SpinEdit1.Value := 2;
 sltb := sldb.GetTable(Format('SELECT * FROM MessagesTexts WHERE message_id = %d ORDER By msgtxt_id',[message_id]));
 i := 0;
 ListBox1.Items.Clear;
 while not sltb.EOF do
  begin
   SetLength(msgTexts , i + 1);
   msgTexts[i].msgtxtId := sltb.FieldAsInteger(sltb.FieldIndex['msgtxt_id']);
   msgTexts[i].message_id := sltb.FieldAsInteger(sltb.FieldIndex['message_id']);
   msgTexts[i].text := sltb.FieldAsString(sltb.FieldIndex['text']);
   msgTexts[i].keep := sltb.FieldAsInteger(sltb.FieldIndex['keep']);
   msgTexts[i].time := sltb.FieldAsInteger(sltb.FieldIndex['time']);
   ListBox1.Items.Add(msgTexts[i].text);
   inc(i);
   sltb.Next;
  end;
except
end;
end;

procedure TfrmMessage.loadMessage;
var sltb : TSQLIteTable;
begin
 sltb := sldb.GetTable(Format('SELECT * FROM MessagesTexts WHERE msgtxt_id = %d',[msgTexts[ListBox1.ItemIndex].msgtxtId]));
 while not sltb.EOF do
  begin
    msgText.msgtxtId := msgTexts[ListBox1.ItemIndex].msgtxtId;
    edit1.Text := sltb.FieldAsString(sltb.FieldIndex['text']);
    CheckBox1.Checked := sltb.FieldAsInteger(sltb.FieldIndex['keep']) = 1;
    SpinEdit1.Value  := sltb.FieldAsInteger(sltb.FieldIndex['time']);
    sltb.Next;
  end;
end;

procedure TfrmMessage.resetMessage;
begin
  msgText.msgtxtId := -1;
 edit1.Text := '';
 CheckBox1.Checked := true;
 SpinEdit1.Value := 2;
end;

procedure TfrmMessage.ListBox1DblClick(Sender: TObject);
begin
 if MessageDlg('Doriti sa stergeti mesajul?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
     sldb.ExecSQL(Format('DELETE FROM MessagesTexts WHERE msgtxt_id = %d;', [msgTexts[ListBox1.ItemIndex].msgtxtId]));
    loadData;
    resetMessage;
  end;
end;

procedure TfrmMessage.ListBox1Click(Sender: TObject);
begin
  loadMessage;
end;

procedure TfrmMessage.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_down then
   begin
     loadMessage;
   end;
end;

procedure TfrmMessage.Button2Click(Sender: TObject);
begin
  resetMessage;
end;

procedure TfrmMessage.Button1Click(Sender: TObject);
begin
 if msgText.msgtxtId > 0 then
  begin
   sldb.ExecSQL(Format('UPDATE MessagesTexts SET text = "%s", keep = %d, time = %d WHERE msgtxt_id = %d;',
           [Edit1.text, IfThen(CheckBox1.Checked, 1,0), SpinEdit1.Value, msgText.msgtxtId]));
  loadData;
  end
 else
  begin
   sldb.ExecSQL(Format('INSERT INTO MessagesTexts(message_id, text, keep, time) VALUES (%d, "%s", %d, %d);',
           [message_id, Edit1.text, IfThen(CheckBox1.Checked, 1,0), SpinEdit1.Value]));
   loadData;
  end;
 resetMessage;
end;

end.
