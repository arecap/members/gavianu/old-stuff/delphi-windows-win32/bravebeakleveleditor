object frmLayers: TfrmLayers
  Left = 478
  Top = 308
  Width = 916
  Height = 619
  Caption = 'frmLayers'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 583
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object lbLayers: TListBox
      Left = 0
      Top = 0
      Width = 185
      Height = 583
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbLayersClick
      OnDblClick = lbLayersDblClick
      OnKeyDown = lbLayersKeyDown
    end
  end
  object Panel2: TPanel
    Left = 185
    Top = 0
    Width = 715
    Height = 583
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 48
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object Label2: TLabel
      Left = 48
      Top = 64
      Width = 18
      Height = 13
      Caption = 'Tip:'
    end
    object Label3: TLabel
      Left = 48
      Top = 120
      Width = 32
      Height = 13
      Caption = 'Image:'
    end
    object img: TImage
      Left = 1
      Top = 184
      Width = 713
      Height = 398
      Align = alBottom
      OnClick = imgClick
    end
    object cbType: TComboBox
      Left = 160
      Top = 56
      Width = 121
      Height = 21
      AutoDropDown = True
      AutoCloseUp = True
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = 'Layer 1'
      Items.Strings = (
        'Layer 1'
        'Layer 2'
        'Layer 3')
    end
    object CheckBox1: TCheckBox
      Left = 48
      Top = 152
      Width = 65
      Height = 17
      Caption = 'Strech'
      TabOrder = 4
      OnClick = CheckBox1Click
    end
    object Button1: TButton
      Left = 384
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Reset'
      TabOrder = 3
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 504
      Top = 16
      Width = 81
      Height = 25
      Caption = 'Save'
      TabOrder = 2
      OnClick = Button2Click
    end
    object eName: TEdit
      Left = 160
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Portable Network Graphics (*.png)|*.png'
    Left = 729
    Top = 64
  end
end
