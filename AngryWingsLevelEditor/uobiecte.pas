unit uobiecte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, entities, Spin, SQLite3, SQLiteTable3, pngimage, mmsystem;

type
  TfrmObiecte = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    lbObjects: TListBox;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    eObjectName: TEdit;
    mDescription: TMemo;
    Button1: TButton;
    Button4: TButton;
    seObjectType: TSpinEdit;
    OpenDialog1: TOpenDialog;
    iMainFrame: TImage;
    CheckBox1: TCheckBox;
    Label5: TLabel;
    ComboBox1: TComboBox;
    Label6: TLabel;
    SpinEdit1: TSpinEdit;
    Button2: TButton;
    ListBox1: TListBox;
    OpenDialog2: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure lbObjectsClick(Sender: TObject);
    procedure lbObjectsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbObjectsDblClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure iMainFrameClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    objects : TObjects;
    sldb: TSQLiteDatabase;
    objIdx : integer;
    image : String;
    sound : String;
    sounds : TObjectSounds;

    procedure loadData;
    procedure loadObject;
  public
    { Public declarations }
  end;

var
  frmObiecte: TfrmObiecte;

implementation

uses uobjecttypes, StrUtils;

{$R *.dfm}

procedure TfrmObiecte.loadData;
var i : integer;
    sltb: TSQLIteTable;
    slDBPath : string;
begin
Button2.Enabled := false;
eObjectName.Text := '';
eObjectName.SetFocus;
mDescription.Lines.Clear;
iMainFrame.Picture := nil;
seObjectType.Value := 0;
image := '';
objIdx := -1;

slDBPath := ExtractFilepath(application.exename)
+ 'angry.sqlite';
sldb := TSQLiteDatabase.Create(slDBPath);

try
 sltb := sldb.GetTable('SELECT * FROM Objects ORDER BY ObjType');
 i := 0;
 lbObjects.Items.Clear;
 while not sltb.EOF do
  begin
   SetLength(objects , i + 1);
   objects[i].id := sltb.FieldAsInteger(sltb.FieldIndex['object_id']);
   objects[i].Name := sltb.FieldAsString(sltb.FieldIndex['Name']);
   objects[i].Description := sltb.FieldAsString(sltb.FieldIndex['Description']);
   objects[i].objectType := sltb.FieldAsInteger(sltb.FieldIndex['ObjType']);
   objects[i].MainFrame := sltb.FieldAsString(sltb.FieldIndex['MainFrame']);
   objects[i].action := sltb.FieldAsInteger(sltb.FieldIndex['action']);
   objects[i].value := sltb.FieldAsInteger(sltb.FieldIndex['value']);
   lbObjects.Items.Add(objects[i].Name);
   inc(i);
   sltb.Next;
  end;
except
end;

end;

procedure TfrmObiecte.loadObject;
var i : integer;
    sltb : TSQLiteTable;
begin
eObjectName.Text := objects[objIdx].Name;
mDescription.Lines.Text := objects[objIdx].Description;
seObjectType.Value := objects[objIdx].objectType;
ComboBox1.ItemIndex := objects[objIdx].action;
SpinEdit1.Value := objects[objIdx].value;
image :=   objects[objIdx].MainFrame;
Button2.Enabled := objects[objIdx].value > 0;
try
 sltb := sldb.GetTable(Format('SELECT * FROM ObjectsSounds WHERE object_id = %d', [objects[objIdx].id]));
 ListBox1.Items.Clear;
 i := 0;
 while not sltb.EOF do
  begin
   SetLength(sounds , i + 1);
   sounds[i].sound_id := sltb.FieldAsInteger(sltb.FieldIndex['sound_id']);
   sounds[i].object_id := sltb.FieldAsInteger(sltb.FieldIndex['object_id']);
   sounds[i].sound := sltb.FieldAsString(sltb.FieldIndex['sound']);
   ListBox1.Items.Add(sounds[i].sound);
   inc(i);
   sltb.Next;
  end;
except
end;

iMainFrame.Picture.LoadFromFile(ExtractFilepath(application.exename) + image);
end;

procedure TfrmObiecte.FormShow(Sender: TObject);
begin
  loadData;
end;

procedure TfrmObiecte.lbObjectsClick(Sender: TObject);
begin
 objIdx := lbObjects.ItemIndex;
 loadObject;
end;

procedure TfrmObiecte.lbObjectsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_down then
   begin
     objIdx := lbObjects.ItemIndex;
     loadObject;
   end;
end;

procedure TfrmObiecte.lbObjectsDblClick(Sender: TObject);
begin
 if MessageDlg('Doriti sa stergeti obiectul?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
     sldb.ExecSQL(Format('DELETE FROM Objects WHERE object_id = %d', [objects[lbObjects.ItemIndex].id]));
    loadData;
  end;
end;

procedure TfrmObiecte.Button4Click(Sender: TObject);
begin
eObjectName.Text := '';
mDescription.Lines.Clear;
iMainFrame.Picture := nil;
seObjectType.Value := 0;
image := '';
objIdx := -1;
Button2.Enabled := false;
end;

procedure TfrmObiecte.Button1Click(Sender: TObject);
var strSQL : String;
begin

 if objIdx = -1 then
  begin
    strSQL := Format('INSERT INTO Objects(Name, ObjType, MainFrame, Description, action, value) VALUES ("%s", %d, "%s", "%s", %d, %d);',
           [eObjectName.Text, seObjectType.Value, image, mDescription.Lines.Text, ComboBox1.ItemIndex, SpinEdit1.Value]);
  end
 else
  begin
    strSQL := Format('UPDATE Objects SET '+
                        'Name = "%s", ObjType = %d, MainFrame = "%s", Description = "%s", action = %d, value = %d' +
                        ' WHERE object_id = %d;',
           [eObjectName.Text, seObjectType.Value, image, mDescription.Lines.Text, ComboBox1.ItemIndex, SpinEdit1.Value, objects[objIdx].id]);
  end;
 sldb.ExecSQL(strSQL);
 loadData;  
end;

procedure TfrmObiecte.CheckBox1Click(Sender: TObject);
begin
 iMainFrame.Stretch := CheckBox1.Checked;
end;

procedure TfrmObiecte.iMainFrameClick(Sender: TObject);
var FromF, ToF: file;
  NumRead, NumWritten: Integer;
  Buf: array[1..2048] of Char;
begin
  if OpenDialog1.Execute then
   begin
     AssignFile(FromF, OpenDialog1.FileName);
     Reset(FromF, 1);
     image :=  AnsiRightStr(openDialog1.FileName, AnsiPos('\', AnsiReverseString(openDialog1.FileName)) - 1);
     AssignFile(ToF, ExtractFilepath(application.exename)+image);
     Rewrite (ToF, 1);
      repeat
        BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
        BlockWrite(ToF, Buf, NumRead, NumWritten);
      until (NumRead = 0) or (NumWritten <> NumRead);
      CloseFile(FromF);
      CloseFile(ToF);
      iMainFrame.Picture.LoadFromFile(ExtractFilepath(application.exename)+image);
   end;
end;

procedure TfrmObiecte.Button2Click(Sender: TObject);
var FromF, ToF: file;
  NumRead, NumWritten: Integer;
  Buf: array[1..2048] of Char;
  i : integer;
begin
  if OpenDialog2.Execute then
   begin
     for i := 0 to OpenDialog2.Files.Count - 1 do
      begin
       AssignFile(FromF, OpenDialog2.Files[i]);
       Reset(FromF, 1);
       sound :=  AnsiRightStr(openDialog2.Files[i], AnsiPos('\', AnsiReverseString(openDialog2.Files[i])) - 1);
       AssignFile(ToF, ExtractFilepath(application.exename)+'Sounds\'+sound);
       Rewrite (ToF, 1);
        repeat
          BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
          BlockWrite(ToF, Buf, NumRead, NumWritten);
        until (NumRead = 0) or (NumWritten <> NumRead);
       CloseFile(FromF);
       CloseFile(ToF);

       sound := AnsiReplaceStr(sound, '.wav', '');
       sldb.ExecSQL(Format('INSERT INTO ObjectsSounds(object_id, sound) VALUES(%d, "%s")', [objects[objIdx].id, sound]));
       ListBox1.Items.Add(sound);
      end;
//      iMainFrame.Picture.LoadFromFile(ExtractFilepath(application.exename)+image);
   end;end;

procedure TfrmObiecte.ListBox1DblClick(Sender: TObject);
begin
   SndPlaySound(PAnsiChar( ExtractFilepath(application.exename)+'Sounds\'+ListBox1.Items[ListBox1.ItemIndex] ), SND_NODEFAULT or snd_ASync) ;
end;

procedure TfrmObiecte.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (key = VK_DELETE) then
  begin
    if MessageDlg('Esti sigur ca vrei sa-l stergi?', mtConfirmation, [mbYes, mbCancel], 0) = mrYes then
     begin
      sldb.ExecSQL(Format('DELETE FROM ObjectsSounds WHERE sound_id = %d', [sounds[ListBox1.ItemIndex].sound_id]));
      DeleteFile(ExtractFilepath(application.exename)+'Sounds\'+ListBox1.Items[ListBox1.ItemIndex]+'.wav');
      ListBox1.Items.Delete(ListBox1.ItemIndex);
      loadObject;
     end;
  end;
end;

end.
