object frmName: TfrmName
  Left = 305
  Top = 117
  Width = 1272
  Height = 845
  Caption = 'AW Level Design'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1256
    Height = 789
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
  end
  object MainMenu1: TMainMenu
    Left = 48
    Top = 16
    object Menu1: TMenuItem
      Caption = 'Menu'
      object Objects1: TMenuItem
        Caption = 'Objects'
        OnClick = Objects1Click
      end
      object Layers: TMenuItem
        Caption = 'Layers'
        OnClick = LayersClick
      end
      object Levels1: TMenuItem
        Caption = 'Levels'
        OnClick = Levels1Click
      end
    end
  end
end
