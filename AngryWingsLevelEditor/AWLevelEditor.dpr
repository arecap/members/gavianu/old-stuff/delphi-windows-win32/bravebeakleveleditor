program AWLevelEditor;

uses
  Forms,
  main in 'main.pas' {frmName},
  SQLite3 in 'Utils\SQLite3.pas',
  SQLiteTable3 in 'Utils\SQLiteTable3.pas',
  uobiecte in 'uobiecte.pas' {frmObiecte},
  uobjecttypes in 'uobjecttypes.pas' {frmObjectTypes},
  entities in 'entities.pas',
  ulevel in 'ulevel.pas' {frmLevel},
  pngextra in 'pngimage\pngextra.pas',
  pngimage in 'pngimage\pngimage.pas',
  pnglang in 'pngimage\pnglang.pas',
  zlibpas in 'pngimage\zlibpas.pas',
  unewlevel in 'unewlevel.pas' {frmNewLevel},
  ulayers in 'ulayers.pas' {frmLayers},
  MPNGImage in 'pngimage\MPNGImage.pas',
  umissions in 'umissions.pas' {frmMissions},
  umessages in 'umessages.pas' {frmMessage};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmName, frmName);
  Application.CreateForm(TfrmObiecte, frmObiecte);
  Application.CreateForm(TfrmObjectTypes, frmObjectTypes);
  Application.CreateForm(TfrmLevel, frmLevel);
  Application.CreateForm(TfrmNewLevel, frmNewLevel);
  Application.CreateForm(TfrmLayers, frmLayers);
  Application.CreateForm(TfrmMissions, frmMissions);
  Application.CreateForm(TfrmMessage, frmMessage);
  Application.Run;
end.
