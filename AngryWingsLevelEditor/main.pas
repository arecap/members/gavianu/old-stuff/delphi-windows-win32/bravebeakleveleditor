unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus;

type
  TfrmName = class(TForm)
    Panel1: TPanel;
    MainMenu1: TMainMenu;
    Menu1: TMenuItem;
    Objects1: TMenuItem;
    Levels1: TMenuItem;
    Layers: TMenuItem;
    procedure Objects1Click(Sender: TObject);
    procedure Levels1Click(Sender: TObject);
    procedure LayersClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmName: TfrmName;

implementation

uses uobjecttypes, uobiecte, ulevel, ulayers;

{$R *.dfm}

procedure TfrmName.Objects1Click(Sender: TObject);
begin
  frmObiecte.Parent := panel1;
  frmObiecte.Top := 0;
  frmObiecte.Left := 0;
  frmObiecte.Show;
end;

procedure TfrmName.Levels1Click(Sender: TObject);
begin
 frmLevel.Parent := panel1;
 frmLevel.Top := 0;
 frmLevel.Left := 0;
 frmLevel.Menu := self.MainMenu1;
 frmLevel.Show;
end;

procedure TfrmName.LayersClick(Sender: TObject);
begin
 frmLayers.Parent := panel1;
 frmLayers.Top := 0;
 frmLayers.Left := 0;
 frmLayers.Show;
end;

end.
