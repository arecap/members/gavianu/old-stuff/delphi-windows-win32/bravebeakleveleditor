unit unewlevel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SQLite3, SQLiteTable3;

type
  TfrmNewLevel = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
        sldb: TSQLiteDatabase;
  public
    { Public declarations }
  end;

var
  frmNewLevel: TfrmNewLevel;

implementation

uses ulevel;

{$R *.dfm}

procedure TfrmNewLevel.Button1Click(Sender: TObject);
begin
 ModalResult := mrYes;
end;

procedure TfrmNewLevel.Button2Click(Sender: TObject);
var strSQL : String;
begin
 strSQL := Format('INSERT INTO Levels(name) VALUES ("%s");',
           [edit1.Text]);
 sldb.ExecSQL(strSQL);
 ModalResult := mrYes;
 frmLevel.LoadMenu;
end;

procedure TfrmNewLevel.FormShow(Sender: TObject);
var slDBPath : string;
begin
slDBPath := ExtractFilepath(application.exename)
+ 'angry.sqlite';
sldb := TSQLiteDatabase.Create(slDBPath);
end;

end.
