unit ulevel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvOfficeImage, entities, SQLite3, SQLiteTable3,
  Buttons, Menus, Spin, AppEvnts, pngimage, contnrs;

type
  TfrmLevel = class(TForm)
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    Panel2: TPanel;
    PopupMenu1: TPopupMenu;
    Delete1: TMenuItem;
    lposition: TLabel;
    SentToBack1: TMenuItem;
    BringtoFront1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    Panel3: TPanel;
    Button2: TButton;
    Button1: TButton;
    seLayersCount: TSpinEdit;
    cbLayers: TComboBox;
    cbChangeLayer: TComboBox;
    pMainFrame: TPanel;
    iMainFrame: TImage;
    cbObjects: TComboBox;
    SpinEdit1: TSpinEdit;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    Button3: TButton;
    CheckBox2: TCheckBox;
    Label2: TLabel;
    SpinEdit2: TSpinEdit;
    Button4: TButton;
    l1: TSpeedButton;
    l2: TSpeedButton;
    l3: TSpeedButton;
    l4: TSpeedButton;
    l5: TSpeedButton;
    l6: TSpeedButton;
    l7: TSpeedButton;
    ComboBox1: TComboBox;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    PopupMenu2: TPopupMenu;
    Edit1: TMenuItem;
    Delete2: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure cbObjectsChange(Sender: TObject);
    procedure imgMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure imgMouseClick(Sender: TObject);    procedure NewLevelClick(Sender: TObject);
    procedure imgMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgLayerClick(Sender : TObject);  
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LevelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure iMainFrameClick(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure Panel2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure cbChangeLayerChange(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SentToBack1Click(Sender: TObject);
    procedure BringtoFront1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure SpinEdit1Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure l1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure ControlMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ControlMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ControlMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PositionNodes(AroundControl: TWinControl);
    procedure SetNodesVisible(Visible: Boolean);
    procedure Button7Click(Sender: TObject);
    procedure CreateNodes;
    procedure NodeMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure NodeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure NodeMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Delete2Click(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
  private
    { Private declarations }

    inReposition : boolean;
    FNodePositioning: Boolean;
    oldPos: TPoint;
    FNodes: TObjectList;
    FCurrentNodeControl: TWinControl;
    
    FMenu : TMainMenu;
    objects : TObjects;
    layers : TGameLayers;
    sldb: TSQLiteDatabase;
    objIdx : integer;

    idxObjectLevel : integer;
    idxMessageLevel : integer;

    selected : boolean;

    levelObjects : TElements;
    levelLayers1 : TElements;
    levelLayers2 : TElements;
    levelLayers3 : TElements;

//    point : tpoint;

    menuItems : array of TMenuItem;

    level_id : integer;
    level_scroll : integer;
    level_name : String;
    imgFromLevel : boolean;
    levelMessages : TMessages;
    procedure loadData(level_id : integer; scroll : integer);
    procedure loadLayers(layerType : integer);
    procedure loadObject;
    procedure loadLavelDesign(level_id : integer; scroll : integer);
    procedure arrangelayers;
    procedure Mirror(Picture: TPicture; MirrorType: TMirror);
    function GraphicToPNG(Graphic:TGraphic):TPngObject;
  public
    { Public declarations }
    level : TLevel;
    procedure LoadMenu;
    property Menu : TMainMenu read FMenu write FMenu;
  end;

var
  frmLevel: TfrmLevel;

implementation

uses Math, unewlevel, StrUtils, umissions, umessages;

{$R *.dfm}

procedure TfrmLevel.loadData(level_id : integer; scroll : integer);
var i : integer;
    sltb: TSQLIteTable;
begin
 SpinEdit1.Value := 100;

Panel1.Visible := true;
objIdx := -1;
imgFromLevel := false;
try
 sltb := sldb.GetTable('SELECT * FROM Objects ORDER BY ObjType');
 i := 0;
 cbObjects.Items.Clear;
 while not sltb.EOF do
  begin
   SetLength(objects , i + 1);
   objects[i].id := sltb.FieldAsInteger(sltb.FieldIndex['object_id']);
   objects[i].Name := sltb.FieldAsString(sltb.FieldIndex['Name']);
   objects[i].Description := sltb.FieldAsString(sltb.FieldIndex['Description']);
   objects[i].objectType := sltb.FieldAsInteger(sltb.FieldIndex['ObjType']);
   objects[i].MainFrame := sltb.FieldAsString(sltb.FieldIndex['MainFrame']);
   cbObjects.Items.Add(objects[i].Name);
   inc(i);
   sltb.Next;
  end;
 if length(objects) > 0 then
  begin
   cbObjects.ItemIndex := 0;
    objIdx := cbObjects.ItemIndex;
    loadObject;
  end;
 sltb := sldb.GetTable(Format('SELECT name, angrylife, leveltype FROM Levels WHERE level_id = %d ORDER By name',[level_id]));
 while not sltb.EOF do
  begin
    level.id := level_id;
    level.name := sltb.FieldAsString(sltb.FieldIndex['name']);
    SpinEdit2.Value := sltb.FieldAsInteger(sltb.FieldIndex['angrylife']);
    level.angrylife := SpinEdit2.Value;
    ComboBox1.ItemIndex := sltb.FieldAsInteger(sltb.FieldIndex['leveltype']);
    level.leveltype := ComboBox1.ItemIndex;
    sltb.Next;
  end;
 Button4.Enabled := false;
 Button5.Enabled := false; 
 cbChangeLayer.ItemIndex := 0;
 loadLayers(0);
 loadLavelDesign(level_id, scroll);
except
end;
end;

procedure TfrmLevel.loadLavelDesign(level_id: integer; scroll : integer);
var i, k : integer;
    sltb: TSQLIteTable;
begin
try

 for i := 0 to high(levelLayers1) do
  begin
    levelLayers1[i].img.Free;
  end;
 for i := 0 to high(levelLayers2) do
  begin
    levelLayers2[i].img.Free;
  end;
 for i := 0 to high(levelLayers3) do
  begin
    levelLayers3[i].img.Free;
  end;
 for i := 0 to high(levelObjects) do
  begin
    levelObjects[i].img.Free;
  end;
 for i := 0 to high(levelMessages) do
  begin
    levelMessages[i].panel.Free;
  end;
 SetNodesVisible(false);
 SetLength(levelLayers1, 0);
 sltb := sldb.GetTable(Format('SELECT le.*, l.main_frame FROM LevelsElements le LEFT JOIN Layers l ON l.layer_id = le.element_id  WHERE level_id = %d AND element_type = 0'+
            ' AND le.left >= %d AND le.left <= %d',[level_id, 30000*scroll, 35000*(scroll+1)]));
 while not sltb.EOF do
  begin
   k :=  length(levelLayers1);
   SetLength(levelLayers1, k + 1);
   levelLayers1[ k ].img := TImage.Create(nil);
   levelLayers1[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + sltb.FieldAsString(sltb.FieldIndex['main_frame']));
   levelLayers1[ k ].img.Top := 3000 - sltb.FieldAsInteger(sltb.FieldIndex['top']);
   levelLayers1[ k ].img.Left := sltb.FieldAsInteger(sltb.FieldIndex['left']) - 32767*scroll;
   levelLayers1[ k ].img.Width := levelLayers1[ k ].img.Picture.Width;
   levelLayers1[ k ].img.Height := levelLayers1[ k ].img.Picture.Height;
   levelLayers1[ k ].img.Parent := Panel2;
   levelLayers1[ k ].img.Stretch := true;
   levelLayers1[ k ].img.OnMouseMove := imgMouseMove;
   levelLayers1[ k ].img.OnClick := imgLayerClick;
   levelLayers1[ k ].id := sltb.FieldAsInteger(sltb.FieldIndex['id']);
   sltb.Next;
  end;

 SetLength(levelLayers2, 0);
 sltb := sldb.GetTable(Format('SELECT le.*, l.main_frame FROM LevelsElements le LEFT JOIN Layers l ON l.layer_id = le.element_id  WHERE level_id = %d AND element_type = 1'+
        ' AND le.left >= %d AND le.left <= %d',[level_id, 30000*scroll, 35000*(scroll+1)]));
 while not sltb.EOF do
  begin
   k :=  length(levelLayers2);
   SetLength(levelLayers2, k + 1);
   levelLayers2[ k ].img := TImage.Create(nil);
   levelLayers2[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + sltb.FieldAsString(sltb.FieldIndex['main_frame']));
   levelLayers2[ k ].img.Top := 3000 - sltb.FieldAsInteger(sltb.FieldIndex['top']);
   levelLayers2[ k ].img.Left := sltb.FieldAsInteger(sltb.FieldIndex['left']) - 32767*scroll;
   levelLayers2[ k ].img.Width := levelLayers2[ k ].img.Picture.Width;
   levelLayers2[ k ].img.Height := levelLayers2[ k ].img.Picture.Height;
   levelLayers2[ k ].img.Parent := Panel2;
   levelLayers2[ k ].img.Stretch := true;
   levelLayers2[ k ].img.OnMouseMove := imgMouseMove;
   levelLayers2[ k ].img.OnClick := imgLayerClick;
   levelLayers2[ k ].id := sltb.FieldAsInteger(sltb.FieldIndex['id']);
   sltb.Next;
  end;

 SetLength(levelLayers3, 0);
 sltb := sldb.GetTable(Format('SELECT le.*, l.main_frame FROM LevelsElements le LEFT JOIN Layers l ON l.layer_id = le.element_id  WHERE level_id = %d AND element_type = 2'+
            ' AND le.left >= %d AND le.left <= %d',[level_id, 30000*scroll, 35000*(scroll+1)]));
 while not sltb.EOF do
  begin
   k :=  length(levelLayers3);
   SetLength(levelLayers3, k + 1);
   levelLayers3[ k ].img := TImage.Create(nil);
   levelLayers3[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + sltb.FieldAsString(sltb.FieldIndex['main_frame']));
   levelLayers3[ k ].img.Top := 3000 - sltb.FieldAsInteger(sltb.FieldIndex['top']);
   levelLayers3[ k ].img.Left := sltb.FieldAsInteger(sltb.FieldIndex['left']) - 32767*scroll;
   levelLayers3[ k ].img.Width := levelLayers3[ k ].img.Picture.Width;
   levelLayers3[ k ].img.Height := levelLayers3[ k ].img.Picture.Height;
   levelLayers3[ k ].img.Parent := Panel2;
   levelLayers3[ k ].img.Stretch := true;
   levelLayers3[ k ].img.OnMouseMove := imgMouseMove;
   levelLayers3[ k ].img.OnClick := imgLayerClick;
   levelLayers3[ k ].id := sltb.FieldAsInteger(sltb.FieldIndex['id']);
   sltb.Next;
  end;

 SetLength(levelObjects, 0);
 sltb := sldb.GetTable(Format('SELECT le.*, o.MainFrame FROM LevelsElements le LEFT JOIN Objects o ON o.object_id = le.element_id  WHERE level_id = %d AND element_type = 3'+
            ' AND le.left >= %d AND le.left <= %d ORDER BY le.z',[level_id, 30000*scroll, 35000*(scroll+1)]));
 while not sltb.EOF do
  begin
   k :=  length(levelObjects);
   SetLength(levelObjects, k + 1);
   levelObjects[ k ].reverse :=  sltb.FieldAsInteger(sltb.FieldIndex['reverse']) = 1;
   levelObjects[ k ].img := TImage.Create(nil);
   levelObjects[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + sltb.FieldAsString(sltb.FieldIndex['MainFrame']));
   levelObjects[ k ].img.Top := 3000 - sltb.FieldAsInteger(sltb.FieldIndex['top']);
   levelObjects[ k ].img.Left := sltb.FieldAsInteger(sltb.FieldIndex['left']) - 32767*scroll;
   levelObjects[ k ].img.Width := levelObjects[ k ].img.Picture.Width;
   levelObjects[ k ].img.Height := levelObjects[ k ].img.Picture.Height;
   levelObjects[ k ].img.Parent := Panel2;
   levelObjects[ k ].img.Stretch := true;
   if(levelObjects[ k ].reverse)then Mirror(levelObjects[ k ].img.Picture, mtHorizontal);
   levelObjects[ k ].id := sltb.FieldAsInteger(sltb.FieldIndex['id']);
   levelObjects[ k ].img.OnMouseMove := imgMouseMove;
   levelObjects[ k ].img.OnClick := imgMouseClick;
   levelObjects[ k ].img.OnMouseDown := imgMouseDown;
   levelObjects[ k ].elementId := sltb.FieldAsInteger(sltb.FieldIndex['element_id']);
   sltb.Next;
  end;

  arrangelayers;
 SetLength(levelMessages, 0);
 sltb := sldb.GetTable(Format('SELECT * FROM Messages WHERE level_id = %d AND  x >= %d AND x <= %d',[level_id, 30000*scroll, 35000*(scroll+1)]));
 while not sltb.EOF do
  begin

    k := length(levelMessages);
    SetLength(levelMessages, k + 1);
    levelMessages[k].panel := TPanel.Create( nil );
    levelMessages[k].panel.Color := clFuchsia;
    levelMessages[k].panel.Left := sltb.FieldAsInteger(sltb.FieldIndex['x']) - 32767*scroll;;
    levelMessages[k].panel.Top := 3000 - sltb.FieldAsInteger(sltb.FieldIndex['y']) - sltb.FieldAsInteger(sltb.FieldIndex['height']);
    levelMessages[k].panel.Width := sltb.FieldAsInteger(sltb.FieldIndex['width']);
    levelMessages[k].panel.Height := sltb.FieldAsInteger(sltb.FieldIndex['height']);
    levelMessages[k].panel.Parent := Panel2;
    levelMessages[k].panel.OnMouseDown := ControlMouseDown;
    levelMessages[k].panel.OnMouseMove := ControlMouseMove;
    levelMessages[k].panel.OnMouseUp := ControlMouseUp;
    levelMessages[k].messageId := sltb.FieldAsInteger(sltb.FieldIndex['message_id']);
   sltb.Next;
  end;
 except
end;
end;

procedure TfrmLevel.loadLayers(layerType: integer);
var i : integer;
    sltb: TSQLIteTable;
begin
try
 sltb := sldb.GetTable(Format('SELECT * FROM Layers WHERE type = %d',[layerType]));
  cbLayers.Clear;
 cbLayers.Items.Clear;
 i := 0;
 while not sltb.EOF do
  begin
    SetLength(layers, i + 1);
    layers[i].id := sltb.FieldAsInteger(sltb.FieldIndex['layer_id']);
    layers[i].Image := sltb.FieldAsString(sltb.FieldIndex['main_frame']);
    layers[i].Name := sltb.FieldAsString(sltb.FieldIndex['Name']);
    layers[i].layerType := sltb.FieldAsInteger(sltb.FieldIndex['type']);
    cbLayers.Items.Add(layers[i].Name);
    inc(i);
    sltb.Next;
  end;
 If length(layers) > 0 then
  cbLayers.ItemIndex := 0;  
except
end;
end;


procedure TfrmLevel.loadObject;
begin
iMainFrame.Picture.LoadFromFile(ExtractFilepath(application.exename) + objects[objIdx].MainFrame);
if CheckBox1.Checked then
 begin
  Mirror(iMainFrame.Picture, mtHorizontal);
 end;
end;

procedure TfrmLevel.FormShow(Sender: TObject);
var slDBPath : string;
    i : integer;
begin
 selected := false;
 pMainFrame.BevelOuter := bvNone;
 Panel1.Visible := false;
 slDBPath := ExtractFilepath(application.exename) + 'angry.sqlite';
 sldb := TSQLiteDatabase.Create(slDBPath);
 for i := 0 to high(levelLayers1) do
  begin
    levelLayers1[i].img.Free;
  end;
 SetLength(levelLayers1, 0);
 for i := 0 to high(levelLayers2) do
  begin
    levelLayers2[i].img.Free;
  end;
 SetLength(levelLayers2, 0);

 for i := 0 to high(levelLayers3) do
  begin
    levelLayers3[i].img.Free;
  end;
 SetLength(levelLayers3, 0);
 for i := 0 to high(levelObjects) do
  begin
    levelObjects[i].img.Free;
  end;
 SetLength(levelObjects, 0);
 for i := 0 to high(levelMessages) do
  begin
    levelMessages[i].panel.Free;
  end;
 SetLength(levelMessages, 0);
 SetNodesVisible(false); 

 LoadMenu;
end;

procedure TfrmLevel.cbObjectsChange(Sender: TObject);
begin
  objIdx := cbObjects.ItemIndex;
  loadObject;
end;

procedure TfrmLevel.imgMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
  var clp : TPoint;
begin
   GetCursorPos(clP);
   clP := ScreenToClient(clP);
 If(selected) then
  begin
    levelObjects[ idxObjectLevel ].img.Left := clP.x - ScrollBox1.Left
            + ScrollBox1.HorzScrollBar.ScrollPos - round(levelObjects[ idxObjectLevel ].img.Width/2);
    levelObjects[ idxObjectLevel ].img.Top := clP.y + ScrollBox1.VertScrollBar.ScrollPos
            - round(levelObjects[ idxObjectLevel ].img.Height/2);
  end;

   lposition.Caption := Format('Position x: %d, y: %d', [clP.x - ScrollBox1.Left
            + ScrollBox1.HorzScrollBar.ScrollPos, 3000- clP.y - ScrollBox1.VertScrollBar.ScrollPos]);
end;

procedure TfrmLevel.imgMouseClick(Sender: TObject);
 var i : integer;
begin
  if selected then
    begin
     if( levelObjects[ idxObjectLevel ].id = 0 ) then
      begin

       sldb.ExecSQL(Format('INSERT INTO LevelsElements(level_id, element_id, element_type, z, left, top, width, height, reverse) VALUES(%d,%d,%d,%d,%d,%d,%d,%d, %d) ',
                      [ level_id, levelObjects[ idxObjectLevel ].elementId, 3, idxObjectLevel, levelObjects[ idxObjectLevel ].img.Left + 32767 * level_scroll,
                       3000 - levelObjects[ idxObjectLevel ].img.Top,
                      levelObjects[ idxObjectLevel ].img.Width, levelObjects[ idxObjectLevel ].img.Height,
                      ifthen(levelObjects[ idxObjectLevel ].reverse, 1, 0)]));
        levelObjects[ idxObjectLevel ].id := sldb.GetLastInsertRowID;
      end
     else
      begin
       sldb.ExecSQL(Format('UPDATE LevelsElements SET left = %d, top = %d WHERE id = %d',
                      [levelObjects[ idxObjectLevel ].img.Left + 32767 * level_scroll,
                       3000 - levelObjects[ idxObjectLevel ].img.Top, levelObjects[ idxObjectLevel ].id]));
      end;
{     idxObjectLevel := -1;
     selected := false;
     pMainFrame.BevelOuter := bvNone;
     imgFromLevel := false;}
      idxObjectLevel :=    length(levelObjects);
      SetLength(levelObjects, idxObjectLevel+1);
      levelObjects[ idxObjectLevel ].reverse := CheckBox1.Checked;
      levelObjects[ idxObjectLevel ].img := TImage.Create(nil);
      levelObjects[ idxObjectLevel ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + objects[objIdx].MainFrame);
      levelObjects[ idxObjectLevel ].img.Left := 0;
      levelObjects[ idxObjectLevel ].img.top := 0;
      levelObjects[ idxObjectLevel ].img.Width := levelObjects[ idxObjectLevel ].img.Picture.Width;
      levelObjects[ idxObjectLevel ].img.Height := levelObjects[ idxObjectLevel ].img.Picture.Height;
      levelObjects[ idxObjectLevel ].img.Parent := Panel2;
      levelObjects[ idxObjectLevel ].img.Stretch := true;
      if(levelObjects[ idxObjectLevel ].reverse)then Mirror(levelObjects[ idxObjectLevel ].img.Picture, mtHorizontal);
      levelObjects[ idxObjectLevel ].img.OnMouseMove :=  imgMouseMove;
      levelObjects[ idxObjectLevel ].img.OnClick :=  imgMouseClick;
      levelObjects[ idxObjectLevel ].img.OnMouseDown :=  imgMouseDown;
      levelObjects[ idxObjectLevel ].id := 0;
      levelObjects[ idxObjectLevel ].elementId := objects[objIdx].id;
      selected := true;
      pMainFrame.BevelOuter := bvLowered;
      arrangelayers;
    end
  else
   begin
     selected := true;
     for i:= 0 to High(levelObjects) do
      begin
        if sender = levelObjects[i].img then
         begin
            imgFromLevel := true;
           idxObjectLevel := i;
           break;
         end;
      end;
   end;
end;

procedure TfrmLevel.NewLevelClick(Sender: TObject);
begin
  self.show;
 frmNewLevel.ShowModal;
end;

procedure TfrmLevel.FormClose(Sender: TObject; var Action: TCloseAction);
var i : integer;
begin
  for i := high(menuItems) downto 0 do
    menuItems[i].Free;
  SetLength(menuItems, 0);
 level_id := 0;
 level_name := '';
end;

procedure TfrmLevel.LevelClick(Sender: TObject);
begin
 level_id := (Sender as TMenuItem).Tag;
 l1.flat := true;
 level_scroll := l1.Tag;
 level_name :=  AnsiReplaceStr((Sender as TMenuItem).Caption, '&', '' );
 self.Caption := 'Levels - '+level_name;
 loadData(level_id, level_scroll);
end;

procedure TfrmLevel.FormCreate(Sender: TObject);
begin
 CreateNodes;
 level_id := 0;
 level_name := '';
end;

procedure TfrmLevel.LoadMenu;
var i : integer;
    sltb: TSQLIteTable;
    menuItem : TMenuItem;
begin

  for i := high(menuItems) downto 0 do
    menuItems[i].Free;
  i := 0;
  SetLength(menuItems, i + 1);
  menuItem := TMenuItem.Create(FMenu);
  menuItem.Caption := 'Levels';
  FMenu.Items.Add(menuItem);
  menuItems[i] := menuItem;
  inc(i);
  SetLength(menuItems, i + 1);
  menuItem := TMenuItem.Create(FMenu);
  menuItem.Caption := 'New level';
  menuItem.OnClick := NewLevelClick;
  menuItems[0].Add(menuItem);
  menuItems[i] := menuItem;
  inc(i);
  sltb := sldb.GetTable('SELECT * FROM Levels ORDER By name');
 while not sltb.EOF do
  begin
   SetLength(menuItems , i + 1);
   menuItem := TMenuItem.Create(FMenu);
   menuItem.Caption := sltb.FieldAsString(sltb.FieldIndex['name']);
   menuItem.Tag := sltb.FieldAsInteger(sltb.FieldIndex['level_id']);
   menuItem.OnClick := LevelClick;
   menuItems[0].Add(menuItem);
   menuItems[i] := menuItem;
   inc(i);
   sltb.Next;
  end;

  case level_id of
    0: self.Caption := 'Levels';
    else
      self.Caption := 'Levels - '+level_name;
  end;
end;

procedure TfrmLevel.iMainFrameClick(Sender: TObject);
begin
 if imgFromLevel then
  exit;
 if selected then
  begin
    If idxObjectLevel <> -1 then
     begin
      levelObjects[idxObjectLevel].img.Free;
      SetLength(levelObjects, Length(levelObjects) - 1);
//      SetLength(levelObjects, Length(levelObjects) - 1);
      idxObjectLevel := -1;
     end;
    selected := false;
    pMainFrame.BevelOuter := bvNone;
  end
 else
  begin
      Button1.SetFocus;
      idxObjectLevel :=    length(levelObjects);
      SetLength(levelObjects, idxObjectLevel+1);
      levelObjects[ idxObjectLevel ].reverse := CheckBox1.Checked;
      levelObjects[ idxObjectLevel ].img := TImage.Create(nil);
      levelObjects[ idxObjectLevel ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + objects[objIdx].MainFrame);
      levelObjects[ idxObjectLevel ].img.Left := 0;
      levelObjects[ idxObjectLevel ].img.top := 0;
      levelObjects[ idxObjectLevel ].img.Width := levelObjects[ idxObjectLevel ].img.Picture.Width;
      levelObjects[ idxObjectLevel ].img.Height := levelObjects[ idxObjectLevel ].img.Picture.Height;
      levelObjects[ idxObjectLevel ].img.Parent := Panel2;
      levelObjects[ idxObjectLevel ].img.Stretch := true;
      if(levelObjects[ idxObjectLevel ].reverse)then Mirror(levelObjects[ idxObjectLevel ].img.Picture, mtHorizontal);
      levelObjects[ idxObjectLevel ].img.OnMouseMove :=  imgMouseMove;
      levelObjects[ idxObjectLevel ].img.OnClick :=  imgMouseClick;
      levelObjects[ idxObjectLevel ].img.OnMouseDown :=  imgMouseDown;
      levelObjects[ idxObjectLevel ].id := 0;
      levelObjects[ idxObjectLevel ].elementId := objects[objIdx].id;
      selected := true;
      pMainFrame.BevelOuter := bvLowered;
      arrangelayers;
  end;
end;

procedure TfrmLevel.Panel2Click(Sender: TObject);
begin
{  if selected then
    begin
     selected := false;
     idxObjectLevel := -1;
     pMainFrame.BevelOuter := bvNone;
    end;
  else
   begin
     selected := true;
     for i:= 0 to High(imgs) do
      begin
        if sender = imgs[i] then
         begin
           idxObjectLevel := i;
           break;
         end;
      end;
   end;}
end;

procedure TfrmLevel.Panel2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var clP : tpoint;
begin
   GetCursorPos(clP);
   clP := ScreenToClient(clP);
 If(selected) then
  begin
    levelObjects[ idxObjectLevel ].img.Left := clP.x - ScrollBox1.Left
            + ScrollBox1.HorzScrollBar.ScrollPos - round(levelObjects[ idxObjectLevel ].img.Width/2);
    levelObjects[ idxObjectLevel ].img.Top := clP.y + ScrollBox1.VertScrollBar.ScrollPos
            - round(levelObjects[ idxObjectLevel ].img.Height/2);

  end;
   lposition.Caption := Format('Position x: %d, y: %d', [clP.x - ScrollBox1.Left
            + ScrollBox1.HorzScrollBar.ScrollPos, 3000- clP.y - ScrollBox1.VertScrollBar.ScrollPos]);
end;

procedure TfrmLevel.cbChangeLayerChange(Sender: TObject);
begin
 loadLayers(cbChangeLayer.ItemIndex);
end;

procedure TfrmLevel.Delete1Click(Sender: TObject);
var j :integer;
begin
            sldb.ExecSQL(Format('DELETE FROM LevelsElements WHERE id = %d ',
                      [ levelObjects[idxObjectLevel].id]));
           levelObjects[idxObjectLevel].img.Free;
           for j := idxObjectLevel + 1 to High(levelObjects) do
             levelObjects[j-1] := levelObjects[j];
           SetLength( levelObjects, Length(levelObjects) - 1);
     idxObjectLevel := -1;
     selected := false;
     pMainFrame.BevelOuter := bvNone;
     imgFromLevel := false;
end;

procedure TfrmLevel.imgMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var i : integer;
    P: TPoint;
begin
  if Button = mbRight then
     for i:= 0 to High(levelObjects) do
      begin
        if sender = levelObjects[i].img then
         begin
           idxObjectLevel := i;
           GetCursorPos(P);
           PopupMenu1.Popup(P.x,P.y);
           break;
         end;
      end;
end;

procedure TfrmLevel.Button1Click(Sender: TObject);
var i, k, startLeft : integer;
begin
 case cbChangeLayer.ItemIndex of
    0: begin
        for i := 1 to seLayersCount.Value do
         begin
            if length(levelLayers1) > 0 then
              startLeft := levelLayers1[high(levelLayers1)].img.Left + levelLayers1[high(levelLayers1)].img.Width
            else
              startLeft := 0;
            k :=  length(levelLayers1);
            SetLength(levelLayers1, k + 1);
            levelLayers1[ k ].img := TImage.Create(nil);
            levelLayers1[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + layers[cbLayers.ItemIndex].Image);
            levelLayers1[ k ].img.Top := 3000 - levelLayers1[ k ].img.Picture.Height;
            levelLayers1[ k ].img.Left := startLeft;
            levelLayers1[ k ].img.Width := levelLayers1[ k ].img.Picture.Width;
            levelLayers1[ k ].img.Height := levelLayers1[ k ].img.Picture.Height;
            levelLayers1[ k ].img.Parent := Panel2;
            levelLayers1[ k ].img.Stretch := true;
            levelLayers1[ k ].img.OnMouseMove := imgMouseMove;
            levelLayers1[ k ].img.OnClick := imgLayerClick;
            sldb.ExecSQL(Format('INSERT INTO LevelsElements(level_id, element_id, element_type, z, left, top, width, height) VALUES(%d,%d,%d,%d,%d,%d,%d,%d) ',
                      [ level_id, layers[cbLayers.ItemIndex].id, 0, 0, levelLayers1[ k ].img.Left + 32767 * level_scroll, 3000 - levelLayers1[ k ].img.Top,
                      levelLayers1[ k ].img.Width, levelLayers1[ k ].img.Height]));
            levelLayers1[ k ].id := sldb.GetLastInsertRowID;
         end;
       end;
    1: begin
        for i := 1 to seLayersCount.Value do
         begin
            if length(levelLayers2) > 0 then
              startLeft := levelLayers2[high(levelLayers2)].img.Left + levelLayers2[high(levelLayers2)].img.Width
            else
              startLeft := 0;
            k :=  length(levelLayers2);
            SetLength(levelLayers2, k + 1);
            levelLayers2[ k ].img := TImage.Create(nil);
            levelLayers2[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + layers[cbLayers.ItemIndex].Image);
            levelLayers2[ k ].img.Top := 3000 - levelLayers2[ k ].img.Picture.Height;
            levelLayers2[ k ].img.Left := startLeft;
            levelLayers2[ k ].img.Width := levelLayers2[ k ].img.Picture.Width;
            levelLayers2[ k ].img.Height := levelLayers2[ k ].img.Picture.Height;
            levelLayers2[ k ].img.Parent := Panel2;
            levelLayers2[ k ].img.Stretch := true;
            levelLayers2[ k ].img.OnMouseMove := imgMouseMove;
            levelLayers2[ k ].img.OnClick := imgLayerClick;
            sldb.ExecSQL(Format('INSERT INTO LevelsElements(level_id, element_id, element_type, z, left, top, width, height) VALUES(%d,%d,%d,%d,%d,%d,%d,%d) ',
                      [ level_id, layers[cbLayers.ItemIndex].id, 1, 0, levelLayers2[ k ].img.Left + 32767 * level_scroll, 3000 - levelLayers2[ k ].img.Top,
                      levelLayers2[ k ].img.Width, levelLayers2[ k ].img.Height]));
            levelLayers2[ k ].id := sldb.GetLastInsertRowID;
         end;
       end;
    2: begin
        for i := 1 to seLayersCount.Value do
         begin
            if length(levelLayers3) > 0 then
              startLeft := levelLayers3[high(levelLayers3)].img.Left + levelLayers3[high(levelLayers3)].img.Width
            else
              startLeft := 0;
            k :=  length(levelLayers3);
            SetLength(levelLayers3, k + 1);
            levelLayers3[ k ].img := TImage.Create(nil);
            levelLayers3[ k ].img.Picture.LoadFromFile(ExtractFilepath(application.exename) + layers[cbLayers.ItemIndex].Image);
            levelLayers3[ k ].img.Top := 3000 - levelLayers3[ k ].img.Picture.Height;
            levelLayers3[ k ].img.Left := startLeft;
            levelLayers3[ k ].img.Width := levelLayers3[ k ].img.Picture.Width;
            levelLayers3[ k ].img.Height := levelLayers3[ k ].img.Picture.Height;
            levelLayers3[ k ].img.Parent := Panel2;
            levelLayers3[ k ].img.Stretch := true;
            levelLayers3[ k ].img.OnMouseMove := imgMouseMove;
            levelLayers3[ k ].img.OnClick := imgLayerClick;
            sldb.ExecSQL(Format('INSERT INTO LevelsElements(level_id, element_id, element_type, z, left, top, width, height) VALUES(%d,%d,%d,%d,%d,%d,%d,%d) ',
                      [ level_id, layers[cbLayers.ItemIndex].id, 2, 0, levelLayers3[ k ].img.Left + 32767 * level_scroll, 3000 - levelLayers3[ k ].img.Top,
                      levelLayers3[ k ].img.Width, levelLayers3[ k ].img.Height]));
            levelLayers3[ k ].id := sldb.GetLastInsertRowID;
         end;
       end;
  end;
 arrangelayers;
end;

procedure TfrmLevel.arrangelayers;
var i : integer;
begin
  for i := 0 to high(levellayers3) do
     levellayers3[i].img.BringToFront;
  for i := 0 to high(levellayers2) do
     levellayers2[i].img.BringToFront;
  for i := 0 to high(levelObjects) do
     levelObjects[i].img.BringToFront;
  for i := 0 to high(levellayers1) do
     levellayers1[i].img.BringToFront;
end;

procedure TfrmLevel.SentToBack1Click(Sender: TObject);
var levelObject : TElement;
    i : integer;
begin
  for i := idxObjectLevel downto 1 do
   begin
   sldb.ExecSQL(Format('UPDATE LevelsElements SET z = %d WHERE id = %d',
                      [i - 1, levelObjects[ i ].id]));
   sldb.ExecSQL(Format('UPDATE LevelsElements SET z = %d WHERE id = %d',
                      [i, levelObjects[ i - 1 ].id]));

    levelObject := levelObjects[i-1];
    levelObjects[i-1] := levelObjects[i];
    levelObjects[i] := levelObject;
   end;
  idxObjectLevel := -1;
  selected := false;
  pMainFrame.BevelOuter := bvNone;
  imgFromLevel := false;
  arrangelayers;
end;

procedure TfrmLevel.BringtoFront1Click(Sender: TObject);
var levelObject : TElement;
    i : integer;
begin
  for i := idxObjectLevel to high(levelObjects) - 1 do
   begin
     sldb.ExecSQL(Format('UPDATE LevelsElements SET z = %d WHERE id = %d',
                      [i + 1, levelObjects[ i ].id]));
     sldb.ExecSQL(Format('UPDATE LevelsElements SET z = %d WHERE id = %d',
                      [i, levelObjects[ i + 1 ].id]));
    levelObject := levelObjects[i+1];
    levelObjects[i+1] := levelObjects[i];
    levelObjects[i] := levelObject;
   end;
     idxObjectLevel := -1;
     selected := false;
     pMainFrame.BevelOuter := bvNone;
     imgFromLevel := false;
  arrangelayers;
end;

procedure TfrmLevel.Button2Click(Sender: TObject);
begin
 case cbChangeLayer.ItemIndex of
    0:begin
        if length(levelLayers1) > 0 then
         begin
            sldb.ExecSQL(Format('DELETE FROM LevelsElements WHERE id = %d ',
                      [ levelLayers1[high(levelLayers1)].id]));
          levelLayers1[high(levelLayers1)].img.Free;
           SetLength(levelLayers1, length(levelLayers1) - 1);
         end;
      end;
    1:begin
        if length(levelLayers2) > 0 then
         begin
            sldb.ExecSQL(Format('DELETE FROM LevelsElements WHERE id = %d ',
                      [ levelLayers2[high(levelLayers2)].id]));
          levelLayers2[high(levelLayers2)].img.Free;
           SetLength(levelLayers2, length(levelLayers2) - 1);
         end;
      end;
    2:begin
        if length(levelLayers3) > 0 then
         begin
            sldb.ExecSQL(Format('DELETE FROM LevelsElements WHERE id = %d ',
                      [ levelLayers3[high(levelLayers3)].id]));
          levelLayers3[high(levelLayers3)].img.Free;
           SetLength(levelLayers3, length(levelLayers3) - 1);
         end;
      end;
  end;
end;

procedure TfrmLevel.imgLayerClick(Sender: TObject);
begin
     if( levelObjects[ idxObjectLevel ].id = 0 ) then
      begin
       sldb.ExecSQL(Format('INSERT INTO LevelsElements(level_id, element_id, element_type, z, left, top, width, height) VALUES(%d,%d,%d,%d,%d,%d,%d,%d) ',
                      [ level_id, levelObjects[ idxObjectLevel ].elementId, 3, idxObjectLevel, levelObjects[ idxObjectLevel ].img.Left + 32767 * level_scroll,
                       3000 - levelObjects[ idxObjectLevel ].img.Top,
                      levelObjects[ idxObjectLevel ].img.Width, levelObjects[ idxObjectLevel ].img.Height]));
        levelObjects[ idxObjectLevel ].id := sldb.GetLastInsertRowID;
      end
     else
      begin
       sldb.ExecSQL(Format('UPDATE LevelsElements SET left = %d, top = %d WHERE id = %d',
                      [levelObjects[ idxObjectLevel ].img.Left + 32767 * level_scroll,
                       3000 - levelObjects[ idxObjectLevel ].img.Top, levelObjects[ idxObjectLevel ].id]));
      end;
     idxObjectLevel := -1;
     selected := false;
     pMainFrame.BevelOuter := bvNone;
     imgFromLevel := false;
end;

procedure TfrmLevel.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
Var
 Control:TControl;
 p:tpoint;
 fwKeys: integer;
begin
 try
 if Msg.message = WM_MOUSEWHEEL then
   begin
    GetCursorPos(p);
    Control:=ControlAtPos(ScreenToClient(p),False,True);
    if Assigned(Control) Then
     begin
      if Control=ScrollBox1 Then
       begin
          Button1.SetFocus;

        fwKeys := LOWORD(Msg.wParam); // key flags
         if (fwKeys = 1) or (fwKeys = 16) then
          begin
           if  Msg.wParam< 0 then
            ScrollBox1.VertScrollBar.Position := ScrollBox1.VertScrollBar.Position + 100
           else
            ScrollBox1.VertScrollBar.Position := ScrollBox1.VertScrollBar.Position - 100;
          end
         else
          begin
           if  Msg.wParam< 0 then
            ScrollBox1.HorzScrollBar.Position := ScrollBox1.HorzScrollBar.Position + 100
           else
            ScrollBox1.HorzScrollBar.Position := ScrollBox1.HorzScrollBar.Position - 100;
          end;
         Handled := False;
        end;
     end;
   end;
 except
 end;  
end;

procedure TfrmLevel.SpinEdit1Change(Sender: TObject);
begin
 levelLayers1[0].img.Width := levelLayers1[0].img.Width div 2;
 levelLayers1[0].img.Height := levelLayers1[0].img.Height div 2;
 levelLayers1[1].img.Width := levelLayers1[1].img.Width div 2;
 levelLayers1[1].img.Height := levelLayers1[1].img.Height div 2;
 if SpinEdit1.Value <> 100 then
   panel3.Enabled := false
 else
   panel3.Enabled := true;
end;

procedure TfrmLevel.Mirror(Picture: TPicture; MirrorType: TMirror);
var
  MemBmp: Graphics.TBitmap;
  Dest: TRect;
begin { Mirror }
  if Assigned(Picture.Graphic) then
  begin
    MemBmp := Graphics.TBitmap.Create;
    try
      MemBmp.PixelFormat := pf24bit;
      MemBmp.HandleType := bmDIB;
      MemBmp.Width := Picture.Graphic.Width;
      MemBmp.Height := Picture.Height;
      MemBmp.Canvas.Draw(0, 0, Picture.Graphic);

      case MirrorType of
        mtHorizontal:
          begin
            //SpiegelnVertikal(MemBmp);
            //SpiegelnHorizontal(MemBmp);
            Dest.Left := MemBmp.Width;
            Dest.Top := 0;
            Dest.Right := -MemBmp.Width;
            Dest.Bottom := MemBmp.Height
          end;
        mtVertical:
          begin
            Dest.Left := 0;
            Dest.Top := MemBmp.Height;
            Dest.Right := MemBmp.Width;
            Dest.Bottom := -MemBmp.Height
          end;
        mtBoth:
          begin
            Dest.Left := MemBmp.Width;
            Dest.Top := MemBmp.Height;
            Dest.Right := -MemBmp.Width;
            Dest.Bottom := -MemBmp.Height
          end;
      end; { case MirrorType }
      StretchBlt(MemBmp.Canvas.Handle, Dest.Left, Dest.Top, Dest.Right, Dest.Bottom,
                 MemBmp.Canvas.Handle, 0, 0, MemBmp.Width, MemBmp.Height,
                 SRCCOPY);

      Picture.Graphic.Assign(GraphicToPNG(MemBmp));
      Invalidate
    finally
      FreeAndNil(MemBmp)
    end; { try }
  end; { Assigned(Picture.Graphic) }
end; { Mirror }

procedure TfrmLevel.CheckBox1Click(Sender: TObject);
begin
 loadObject;
end;

function TfrmLevel.GraphicToPNG(Graphic: TGraphic): TPngObject;
var
  r,b:Graphics.TBitmap;
  x,y:Integer;
  t,n,l:PByteArray;
  c:TColor;
  a:Byte;

  procedure FC(RR,RG,RB,BR,BG,BB:Byte);
  var r,g,b:Byte;
  begin
  if (RR = 255) and (RG = 255) and ( RB = 255) then
   begin
    a:=0;
    r:=0;
    g:=0;
    b:=0;
   end
  else if RGB(RR,RG,RB)=RGB(BR,BG,BB) then
    begin
    a:=255;
    r:=RR;
    g:=RG;
    b:=RB;
    end
  else if (RR=255) and (BB=255) and (RG+RB+BR+BG=0) then
    begin
    a:=0;
    r:=0;
    g:=0;
    b:=0;
    end
  else
    begin
    a:=BR-RR+255;
    r:=BR*255 div a;
    g:=BG*255 div a;
    b:=RB*255 div a;
    end;
  c:=RGB(r,g,b);
  end;

begin
r:=Graphics.TBitmap.Create;
r.Canvas.Pen.Color:=clRed;
r.Canvas.Brush.Color:=clRed;
r.Width:=Graphic.Width;
r.Height:=Graphic.Height;
r.Canvas.FillRect(r.Canvas.ClipRect);
r.Canvas.Draw(0,0,Graphic);
r.PixelFormat:=pf24bit;
b:=Graphics.TBitmap.Create;
b.Canvas.Pen.Color:=clBlue;
b.Canvas.Brush.Color:=clBlue;
b.Width:=Graphic.Width;
b.Height:=Graphic.Height;
b.Canvas.FillRect(r.Canvas.ClipRect);
b.Canvas.Draw(0,0,Graphic);
b.PixelFormat:=pf24bit;
Result:=TPngObject.CreateBlank(COLOR_RGBALPHA,8,Graphic.Width,Graphic.Height);
for y:=Graphic.Height-1 downto 0 do
  begin
  t:=r.ScanLine[y];
  n:=b.ScanLine[y];
  l:=Result.AlphaScanline[y];
  for x:=0 to Graphic.Width-1 do
    begin
    FC(t^[x*3+2],t^[x*3+1],t^[x*3],n^[x*3+2],n^[x*3+1],n^[x*3]);
    Result.Pixels[x,y]:=c;
    l^[x]:=a;
    end;
  end;
r.Free;
b.Free;
end;

procedure TfrmLevel.Button3Click(Sender: TObject);
begin
 if MessageDlg('Esti sigur ca veri sa stergi elementele din nivel?', mtConfirmation, [mbYes, mbCancel], 0) = mrYes then
  begin
  sldb.ExecSQL(Format('DELETE FROM LevelsElements WHERE level_id = %d',[level_id]));
   loadData(level_id, level_scroll);
  end;
end;

procedure TfrmLevel.CheckBox2Click(Sender: TObject);
var i : integer;
begin
 for i := 0 to high(levelLayers1) do begin
    levelLayers1[i].img.Visible := not CheckBox2.Checked;
    levelLayers1[i].img.Enabled := not CheckBox2.Checked;
 end;
end;

procedure TfrmLevel.SpinEdit2Change(Sender: TObject);
begin
 Button4.Enabled := true;
end;

procedure TfrmLevel.Button4Click(Sender: TObject);
begin
  sldb.ExecSQL(Format('UPDATE Levels SET angrylife = %d WHERE level_id = %d',
                      [SpinEdit2.Value, level_id]));
  level.angrylife :=  SpinEdit2.Value;
  Button4.Enabled := false;

end;

procedure TfrmLevel.l1Click(Sender: TObject);
begin
 l1.flat := false;
 l2.flat := false;
 l3.flat := false;
 l4.flat := false;
 l5.flat := false;
 l6.flat := false;
 l7.flat := false;
 CheckBox2.Checked := false;
 (Sender as TSpeedButton).flat := true;
  level_scroll := (Sender as TSpeedButton).Tag;
 loadLavelDesign(level_id, level_scroll);
end;

procedure TfrmLevel.ComboBox1Change(Sender: TObject);
begin
 Button5.Enabled := true;
end;

procedure TfrmLevel.Button5Click(Sender: TObject);
begin
  sldb.ExecSQL(Format('UPDATE Levels SET leveltype = %d WHERE level_id = %d',
                      [ComboBox1.ItemIndex, level_id]));
  level.leveltype := ComboBox1.ItemIndex;
  Button5.Enabled := false;
end;

procedure TfrmLevel.Button6Click(Sender: TObject);
begin
frmMissions.level := level;
frmMissions.sldb := sldb;
frmMissions.objects := objects;
frmMissions.loadMissions;
frmMissions.ShowModal;
end;

procedure TfrmLevel.ControlMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var i : integer;
    P: TPoint;
begin
  idxMessageLevel := -1;
     for i:= 0 to High(levelMessages) do
      begin
        if sender = levelMessages[i].panel then
         begin
           idxMessageLevel := i;
           break;
         end;
      end;
  if Button = mbRight then
   begin
           GetCursorPos(P);
           PopupMenu2.Popup(P.x,P.y);
    end
   else
    begin
  if (Sender is TWinControl) then
  begin
    inReposition:=True;
    SetCapture(TWinControl(Sender).Handle);
    GetCursorPos(oldPos);
    PositionNodes(TWinControl(Sender));
  end;
    end;
end;

procedure TfrmLevel.ControlMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
const
  minWidth = 20;
  minHeight = 20;
var
  newPos: TPoint;
  frmPoint : TPoint;
begin
  if inReposition then
  begin
    with TWinControl(Sender) do
    begin
      GetCursorPos(newPos);
      Screen.Cursor := crSize;
      Left := Left - oldPos.X + newPos.X;
      Top := Top - oldPos.Y + newPos.Y;
      oldPos := newPos;
    end;
    PositionNodes(TWinControl(Sender));
  end;
end;

procedure TfrmLevel.ControlMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if inReposition then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    inReposition := False;
     sldb.ExecSQL(Format('UPDATE Messages SET x = %d, y = %d, width = %d, height = %d WHERE message_id = %d',
                      [ (sender as Tpanel).Left + 32767 * level_scroll, 3000 - (sender as Tpanel).Top - (sender as Tpanel).Height,
                      (sender as Tpanel).Width, (sender as Tpanel).Height, levelMessages[ idxMessageLevel ].messageId]));

  end;
end;

procedure TfrmLevel.PositionNodes(AroundControl: TWinControl);
var
  Node,T,L,CT,CL,FR,FB,FT,FL: Integer;
  TopLeft: TPoint;
begin
  FCurrentNodeControl := nil;
  for Node := 0 to 7 do
  begin
    with AroundControl do
    begin
      CL := (Width div 2) + Left -2;
      CT := (Height div 2) + Top -2;
      FR := Left + Width - 2;
      FB := Top + Height - 2;
      FT := Top - 2;
      FL := Left - 2;
      case Node of
        0: begin
             T := FT;
             L := FL;
           end;
        1: begin
             T := FT;
             L := CL;
           end;
        2: begin
             T := FT;
             L := FR;
           end;
        3: begin
             T := CT;
             L := FR;
           end;
        4: begin
             T := FB;
             L := FR;
           end;
        5: begin
             T := FB;
             L := CL;
           end;
        6: begin
             T := FB;
             L := FL;
           end;
        7: begin
             T := CT;
             L := FL;
           end;
        else
          T := 0;
          L := 0;
      end;
      TopLeft := Parent.ClientToScreen(Point(L,T));
    end;
   with TPanel(FNodes[Node]) do
    begin
      TopLeft := Parent.ScreenToClient(TopLeft);
      Top := TopLeft.Y;
      Left := TopLeft.X;
    end;
  end;
  FCurrentNodeControl := AroundControl;
  SetNodesVisible(True);end;

procedure TfrmLevel.SetNodesVisible(Visible: Boolean);
var
  Node: Integer;
begin
  for Node := 0 to 7 do
    TWinControl(FNodes.Items[Node]).Visible := Visible;
end;

procedure TfrmLevel.Button7Click(Sender: TObject);
var k : integer;
begin
 k := length(levelMessages);
 SetLength(levelMessages, k + 1);
 levelMessages[k].panel := TPanel.Create( nil );
 levelMessages[k].panel.Left := ScrollBox1.HorzScrollBar.Position + 10;
 levelMessages[k].panel.Top := ScrollBox1.VertScrollBar.Position + 10;
 levelMessages[k].panel.Color := clFuchsia;
 levelMessages[k].panel.Width := 100;
 levelMessages[k].panel.Height := 100;
 levelMessages[k].panel.Parent := Panel2;
 levelMessages[k].panel.OnMouseDown := ControlMouseDown;
 levelMessages[k].panel.OnMouseMove := ControlMouseMove;
 levelMessages[k].panel.OnMouseUp := ControlMouseUp;
 sldb.ExecSQL(Format('INSERT INTO Messages(level_id, x, y, width, height) VALUES(%d,%d,%d,%d,%d)',
                      [ level_id, 10, 3000 - 10 - 1000, 100, 100]));
 levelMessages[k].messageId := sldb.GetLastInsertRowID;
end;

procedure TfrmLevel.CreateNodes;
var
  Node: Integer;
  Panel: TPanel;
begin
  FNodes := TObjectList.Create(False);

  for Node := 0 to 7 do
  begin
    Panel := TPanel.Create(Self);
    FNodes.Add(Panel);
    with Panel do
    begin
      BevelOuter := bvNone;
      Color := clBlack;
      Name := 'Node' + IntToStr(Node);
      Width := 5;
      Height := 5;
      Parent := Panel2;
      Visible := False;

      case Node of
        0,4: Cursor := crSizeNWSE;
        1,5: Cursor := crSizeNS;
        2,6: Cursor := crSizeNESW;
        3,7: Cursor := crSizeWE;
      end;
      OnMouseDown := NodeMouseDown;
      OnMouseMove := NodeMouseMove;
      OnMouseUp := NodeMouseUp;
    end;
  end;

end;

procedure TfrmLevel.NodeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Sender is TWinControl) then
  begin
    FNodePositioning:=True;
    SetCapture(TWinControl(Sender).Handle);
    GetCursorPos(oldPos);
  end;
end;

procedure TfrmLevel.NodeMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
const
  minWidth = 20;
  minHeight = 20;
var
  newPos: TPoint;
  frmPoint : TPoint;
  OldRect: TRect;
  AdjL,AdjR,AdjT,AdjB: Boolean;
begin
  if FNodePositioning then
  begin
    begin
      with TWinControl(Sender) do
      begin
      GetCursorPos(newPos);
      with FCurrentNodeControl do
      begin //resize
        frmPoint := FCurrentNodeControl.Parent.ScreenToClient(Mouse.CursorPos);
        OldRect := FCurrentNodeControl.BoundsRect;
        AdjL := False;
        AdjR := False;
        AdjT := False;
        AdjB := False;
        case FNodes.IndexOf(TWinControl(Sender)) of
          0: begin
               AdjL := True;
               AdjT := True;
             end;
          1: begin
               AdjT := True;
             end;
          2: begin
               AdjR := True;
               AdjT := True;
             end;
          3: begin
               AdjR := True;
             end;
          4: begin
               AdjR := True;
               AdjB := True;
             end;
          5: begin
               AdjB := True;
             end;
          6: begin
               AdjL := True;
               AdjB := True;
             end;
          7: begin
               AdjL := True;
             end;
        end;

        if AdjL then
          OldRect.Left := frmPoint.X;
        if AdjR then
          OldRect.Right := frmPoint.X;
        if AdjT then
          OldRect.Top := frmPoint.Y;
        if AdjB then
          OldRect.Bottom := frmPoint.Y;
        SetBounds(OldRect.Left,OldRect.Top,OldRect.Right - OldRect.Left,OldRect.Bottom - OldRect.Top);
      end;
      Left := Left - oldPos.X + newPos.X;
      Top := Top - oldPos.Y + newPos.Y;
      oldPos := newPos;
      end;
    end;
    PositionNodes(FCurrentNodeControl);
  end;
end;

procedure TfrmLevel.NodeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FNodePositioning then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    FNodePositioning := False;
  end;
end;

procedure TfrmLevel.Delete2Click(Sender: TObject);
var j :integer;
begin
            sldb.ExecSQL(Format('DELETE FROM MessagesTexts WHERE message_id = %d ',
                      [ levelMessages[idxMessageLevel].messageId]));
            sldb.ExecSQL(Format('DELETE FROM Messages WHERE message_id = %d ',
                      [ levelMessages[idxMessageLevel].messageId]));
           levelMessages[idxMessageLevel].panel.Free;
           for j := idxMessageLevel + 1 to High(levelMessages) do
             levelMessages[j-1] := levelMessages[j];
           SetLength( levelMessages, Length(levelMessages) - 1);
     idxMessageLevel := -1;
     SetNodesVisible(false);
end;

procedure TfrmLevel.Edit1Click(Sender: TObject);
begin
 frmMessage.resetMessage;
 frmMessage.message_id :=  levelMessages[idxMessageLevel].messageId;
 frmMessage.sldb := sldb;
 frmMessage.loadData;
 frmMessage.ShowModal;
end;

end.
