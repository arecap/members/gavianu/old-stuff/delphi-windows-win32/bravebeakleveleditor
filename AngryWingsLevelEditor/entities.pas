unit entities;

interface

  uses ExtCtrls;

  	

type
  TMirror = (mtHorizontal, mtVertical, mtBoth );

 type TGameObject = record
    id, objectType, action, value : integer;
    Name, MainFrame, Description : String;
  end;

 type TGameLayer = record
    id, layerType: integer;
    Name, Image: String;
  end;

 type TElement = record
    id, elementId : integer;
    reverse : boolean;
    img: TImage;
  end;

  type TLevel = record
    id,angrylife, leveltype : integer;
    name: string;
   end;

  type TMission = record
      id, level_id, status, mission_type, value :integer;
      cumul : boolean;
      task:string;
    end;

   type TMissionObject = record
    mission_id, object_id : integer;
    objName  : string;
   end;
  type TTextMessage = record
    msgtxtId, message_id, keep, time:integer;
    text: string;
   end;
  type TMessage = record
    messageId, levelId, x, y, width, height : integer;
    panel : TPanel;
    msgTexts : array of TTextMessage;
  end;
  type TObjectSound = record
     object_id, sound_id : integer;
     sound: string;
    end;


 type TObjects = array of TGameObject;
 type TGameLayers = array of TGameLayer;
 type TElements = array of TElement;
 type TMissions = array of TMission;
 type TMissionObjects = array of TMissionObject;
 type TMessages = array of TMessage;
 type TObjectSounds = array of TObjectSound;

implementation

end.
