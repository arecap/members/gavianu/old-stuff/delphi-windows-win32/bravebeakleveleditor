object frmObjectTypes: TfrmObjectTypes
  Left = 433
  Top = 249
  Width = 611
  Height = 353
  Caption = 'Tipuri de obiecte'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 317
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 233
      Align = alTop
      TabOrder = 0
      object lbObjectTypes: TListBox
        Left = 1
        Top = 1
        Width = 183
        Height = 231
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object Button2: TButton
      Left = 8
      Top = 248
      Width = 169
      Height = 25
      Caption = 'Adauga obiect'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 8
      Top = 280
      Width = 169
      Height = 25
      Caption = 'Renunta'
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object Panel3: TPanel
    Left = 185
    Top = 0
    Width = 410
    Height = 317
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 32
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Nume:'
    end
    object Label2: TLabel
      Left = 32
      Top = 64
      Width = 48
      Height = 13
      Caption = 'Descriere:'
    end
    object Label3: TLabel
      Left = 32
      Top = 176
      Width = 61
      Height = 13
      Caption = 'Object Type:'
    end
    object eName: TEdit
      Left = 120
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object mDescription: TMemo
      Left = 120
      Top = 56
      Width = 233
      Height = 105
      TabOrder = 1
    end
    object seIndex: TSpinEdit
      Left = 120
      Top = 176
      Width = 57
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 0
    end
    object Button1: TButton
      Left = 232
      Top = 256
      Width = 123
      Height = 25
      Caption = 'Salveaza'
      TabOrder = 3
    end
    object Button4: TButton
      Left = 88
      Top = 256
      Width = 75
      Height = 25
      Caption = 'Reset'
      TabOrder = 4
    end
  end
end
