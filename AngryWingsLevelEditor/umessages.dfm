object frmMessage: TfrmMessage
  Left = 253
  Top = 124
  Width = 614
  Height = 440
  Caption = 'Message'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 24
    Height = 13
    Caption = 'Text:'
  end
  object Label2: TLabel
    Left = 16
    Top = 72
    Width = 26
    Height = 13
    Caption = 'Timp:'
  end
  object Edit1: TEdit
    Left = 56
    Top = 8
    Width = 401
    Height = 21
    TabOrder = 0
  end
  object CheckBox1: TCheckBox
    Left = 16
    Top = 40
    Width = 97
    Height = 17
    Caption = 'Keep'
    TabOrder = 1
  end
  object SpinEdit1: TSpinEdit
    Left = 56
    Top = 67
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 0
  end
  object Button1: TButton
    Left = 208
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 296
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Reset'
    TabOrder = 4
    OnClick = Button2Click
  end
  object ListBox1: TListBox
    Left = 0
    Top = 114
    Width = 598
    Height = 290
    Align = alBottom
    ItemHeight = 13
    TabOrder = 5
    OnClick = ListBox1Click
    OnDblClick = ListBox1DblClick
    OnKeyDown = ListBox1KeyDown
  end
end
