object frmMissions: TfrmMissions
  Left = 253
  Top = 124
  Width = 964
  Height = 613
  Caption = 'Missions Edit'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 281
    Height = 577
    Align = alLeft
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 40
      Width = 29
      Height = 13
      Caption = 'Level:'
    end
    object Label2: TLabel
      Left = 8
      Top = 80
      Width = 57
      Height = 13
      Caption = 'Viata Angry:'
    end
    object Label3: TLabel
      Left = 8
      Top = 120
      Width = 40
      Height = 13
      Caption = 'Tip nivel'
    end
    object Label14: TLabel
      Left = 16
      Top = 184
      Width = 38
      Height = 13
      Caption = 'Label14'
    end
    object Label15: TLabel
      Left = 96
      Top = 184
      Width = 38
      Height = 13
      Caption = 'Label15'
    end
    object Edit1: TEdit
      Left = 96
      Top = 32
      Width = 97
      Height = 21
      TabOrder = 0
      Text = 'Edit1'
    end
    object SpinEdit1: TSpinEdit
      Left = 96
      Top = 72
      Width = 97
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
    object ComboBox1: TComboBox
      Left = 40
      Top = 144
      Width = 145
      Height = 21
      AutoDropDown = True
      AutoCloseUp = True
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        'Plaja'
        'Munte '
        'Deal')
    end
    object Button1: TButton
      Left = 176
      Top = 184
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 3
      OnClick = Button1Click
    end
    object ListBox1: TListBox
      Left = 1
      Top = 232
      Width = 279
      Height = 344
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 4
      OnClick = ListBox1Click
      OnDblClick = ListBox1DblClick
      OnKeyDown = ListBox1KeyDown
    end
  end
  object Panel2: TPanel
    Left = 281
    Top = 0
    Width = 667
    Height = 577
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object Label4: TLabel
      Left = 24
      Top = 24
      Width = 35
      Height = 13
      Caption = 'Mission'
    end
    object Label5: TLabel
      Left = 24
      Top = 104
      Width = 18
      Height = 13
      Caption = 'Tip:'
    end
    object Label6: TLabel
      Left = 24
      Top = 136
      Width = 39
      Height = 13
      Caption = 'Valoare:'
    end
    object Edit2: TEdit
      Left = 24
      Top = 48
      Width = 489
      Height = 21
      TabOrder = 0
    end
    object Button2: TButton
      Left = 512
      Top = 536
      Width = 145
      Height = 25
      Caption = 'Save'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 368
      Top = 536
      Width = 123
      Height = 25
      Caption = 'Reset'
      TabOrder = 2
      OnClick = Button3Click
    end
    object CheckBox1: TCheckBox
      Left = 24
      Top = 80
      Width = 73
      Height = 17
      Caption = 'Cumul'
      TabOrder = 3
    end
    object ComboBox2: TComboBox
      Left = 64
      Top = 104
      Width = 145
      Height = 21
      AutoDropDown = True
      AutoCloseUp = True
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 4
      Text = 'Scor nivel'
      OnChange = ComboBox2Change
      Items.Strings = (
        'Scor nivel'
        'Obiecte'
        'Rect nivel'
        'Distanta coordonata X'
        'Lansare'
        'Luna'
        'Mini Angry'
        'Double rainbow'
        'Combo slap'
        'Damage points')
    end
    object SpinEdit2: TSpinEdit
      Left = 88
      Top = 136
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 5
      Value = 0
    end
    object pObiecte: TPanel
      Left = 0
      Top = 224
      Width = 665
      Height = 281
      TabOrder = 6
      object Label7: TLabel
        Left = 8
        Top = 24
        Width = 34
        Height = 13
        Caption = 'Obiect:'
      end
      object Image1: TImage
        Left = 16
        Top = 64
        Width = 273
        Height = 185
      end
      object ComboBox3: TComboBox
        Left = 56
        Top = 24
        Width = 137
        Height = 21
        AutoDropDown = True
        AutoCloseUp = True
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = ComboBox3Change
      end
      object ListBox2: TListBox
        Left = 368
        Top = 16
        Width = 281
        Height = 233
        ItemHeight = 13
        TabOrder = 1
        OnDblClick = ListBox2DblClick
      end
      object Button4: TButton
        Left = 304
        Top = 88
        Width = 57
        Height = 25
        Caption = '>>'
        TabOrder = 2
        OnClick = Button4Click
      end
    end
    object pRect: TPanel
      Left = 232
      Top = 80
      Width = 433
      Height = 137
      TabOrder = 7
      object Label8: TLabel
        Left = 8
        Top = 8
        Width = 10
        Height = 13
        Caption = 'X:'
      end
      object Label9: TLabel
        Left = 8
        Top = 40
        Width = 10
        Height = 13
        Caption = 'Y:'
      end
      object Label10: TLabel
        Left = 8
        Top = 72
        Width = 34
        Height = 13
        Caption = 'Latime:'
      end
      object Label11: TLabel
        Left = 8
        Top = 104
        Width = 39
        Height = 13
        Caption = 'Inaltime:'
      end
      object Bevel1: TBevel
        Left = 192
        Top = 16
        Width = 145
        Height = 97
      end
      object Label12: TLabel
        Left = 176
        Top = 120
        Width = 22
        Height = 13
        Caption = '(x, y)'
      end
      object Label13: TLabel
        Left = 320
        Top = 0
        Width = 79
        Height = 13
        Caption = '(Latime, Inaltime)'
      end
      object SpinEdit3: TSpinEdit
        Left = 64
        Top = 8
        Width = 73
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
      end
      object SpinEdit4: TSpinEdit
        Left = 64
        Top = 40
        Width = 73
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 0
      end
      object SpinEdit5: TSpinEdit
        Left = 64
        Top = 72
        Width = 73
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 0
      end
      object SpinEdit6: TSpinEdit
        Left = 64
        Top = 104
        Width = 73
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 0
      end
    end
  end
end
