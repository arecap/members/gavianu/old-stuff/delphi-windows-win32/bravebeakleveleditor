unit uobjecttypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin, SQLite3, SQLiteTable3, entities;

type
  TfrmObjectTypes = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    lbObjectTypes: TListBox;
    Button2: TButton;
    Button3: TButton;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    eName: TEdit;
    mDescription: TMemo;
    seIndex: TSpinEdit;
    Button1: TButton;
    Button4: TButton;
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmObjectTypes: TfrmObjectTypes;

implementation

uses uobiecte;

{$R *.dfm}

procedure TfrmObjectTypes.Button3Click(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmObjectTypes.Button2Click(Sender: TObject);
begin
  try
    frmObiecte.ShowModal;
  except
  end;
end;

end.
