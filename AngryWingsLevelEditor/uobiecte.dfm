object frmObiecte: TfrmObiecte
  Left = 207
  Top = 189
  Width = 933
  Height = 491
  Caption = 'Obiecte'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 455
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 455
      Align = alClient
      TabOrder = 0
      object lbObjects: TListBox
        Left = 1
        Top = 1
        Width = 183
        Height = 453
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbObjectsClick
        OnDblClick = lbObjectsDblClick
        OnKeyDown = lbObjectsKeyDown
      end
    end
  end
  object Panel3: TPanel
    Left = 185
    Top = 0
    Width = 732
    Height = 455
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 32
      Top = 32
      Width = 31
      Height = 13
      Caption = 'Nume:'
    end
    object Label2: TLabel
      Left = 32
      Top = 64
      Width = 18
      Height = 13
      Caption = 'Tip:'
    end
    object Label3: TLabel
      Left = 32
      Top = 96
      Width = 74
      Height = 13
      Caption = 'Frame principal:'
    end
    object Label4: TLabel
      Left = 32
      Top = 224
      Width = 48
      Height = 13
      Caption = 'Descriere:'
    end
    object iMainFrame: TImage
      Left = 152
      Top = 96
      Width = 265
      Height = 105
      OnClick = iMainFrameClick
    end
    object Label5: TLabel
      Left = 40
      Top = 336
      Width = 39
      Height = 13
      Caption = 'Actiune:'
    end
    object Label6: TLabel
      Left = 40
      Top = 368
      Width = 39
      Height = 13
      Caption = 'Valoare:'
    end
    object eObjectName: TEdit
      Left = 160
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object mDescription: TMemo
      Left = 152
      Top = 216
      Width = 209
      Height = 97
      TabOrder = 2
    end
    object Button1: TButton
      Left = 240
      Top = 408
      Width = 131
      Height = 25
      Caption = 'Salveaza'
      TabOrder = 3
      OnClick = Button1Click
    end
    object Button4: TButton
      Left = 72
      Top = 408
      Width = 129
      Height = 25
      Caption = 'Reset'
      TabOrder = 4
      OnClick = Button4Click
    end
    object seObjectType: TSpinEdit
      Left = 160
      Top = 56
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
    object CheckBox1: TCheckBox
      Left = 32
      Top = 120
      Width = 97
      Height = 17
      Caption = 'Strech'
      TabOrder = 5
      OnClick = CheckBox1Click
    end
    object ComboBox1: TComboBox
      Left = 152
      Top = 336
      Width = 145
      Height = 21
      AutoDropDown = True
      AutoCloseUp = True
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 6
      Text = 'Nimic'
      Items.Strings = (
        'Nimic'
        'Puncte'
        'Viata')
    end
    object SpinEdit1: TSpinEdit
      Left = 152
      Top = 368
      Width = 145
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 7
      Value = 0
    end
    object Button2: TButton
      Left = 464
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Load sound'
      TabOrder = 8
      OnClick = Button2Click
    end
    object ListBox1: TListBox
      Left = 464
      Top = 72
      Width = 241
      Height = 321
      ItemHeight = 13
      TabOrder = 9
      OnDblClick = ListBox1DblClick
      OnKeyDown = ListBox1KeyDown
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Portable Network Graphics (*.png)|*.png'
    Left = 561
    Top = 48
  end
  object OpenDialog2: TOpenDialog
    Filter = 'wav (*.wav)|*.wav'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 609
    Top = 56
  end
end
