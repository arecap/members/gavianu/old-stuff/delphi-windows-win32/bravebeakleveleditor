unit umissions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin, entities, SQLite3, SQLiteTable3;

type
  TfrmMissions = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    ComboBox1: TComboBox;
    Button1: TButton;
    Label4: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    ListBox1: TListBox;
    Button3: TButton;
    CheckBox1: TCheckBox;
    Label5: TLabel;
    ComboBox2: TComboBox;
    Label6: TLabel;
    SpinEdit2: TSpinEdit;
    pObiecte: TPanel;
    Label7: TLabel;
    ComboBox3: TComboBox;
    Image1: TImage;
    ListBox2: TListBox;
    pRect: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    Bevel1: TBevel;
    Label12: TLabel;
    Label13: TLabel;
    Button4: TButton;
    Label14: TLabel;
    Label15: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListBox1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ListBox2DblClick(Sender: TObject);
  private
    { Private declarations }
    mission : TMission;
    missionObjects : TMissionObjects;
    missionLoad : boolean;
    procedure loadData;
    procedure loadMission;
    procedure resetMission;
    procedure loadMissionObjects;
  public
    { Public declarations }
    level : TLevel;
    sldb: TSQLiteDatabase;
    missions : TMissions;
    objects : TObjects;
    procedure loadMissions;
  end;

var
  frmMissions: TfrmMissions;

implementation

uses ulevel, Math;

{$R *.dfm}



{ TfrmMissions }

procedure TfrmMissions.loadMissions;
var sltb: TSQLIteTable;
    mini1, mini3: integer;
begin
 sltb := sldb.GetTable(Format('SELECT count(*) as collect FROM LevelsElements WHERE level_id = %d AND element_type = 3 AND element_id = 68',[level.id]));
 while not sltb.EOF do
  begin
   Label14.Caption := IntToStr(sltb.FieldAsInteger(sltb.FieldIndex['collect']));
   sltb.Next;
  end;
 mini1 := 0;
 sltb := sldb.GetTable(Format('SELECT count(*) as collect FROM LevelsElements WHERE level_id = %d AND element_type = 3 AND element_id = 101',[level.id]));
 while not sltb.EOF do
  begin
   mini1 := sltb.FieldAsInteger(sltb.FieldIndex['collect']);
   sltb.Next;
  end;
 mini3 := 0;
 sltb := sldb.GetTable(Format('SELECT count(*) as collect FROM LevelsElements WHERE level_id = %d AND element_type = 3 AND element_id = 102',[level.id]));
 while not sltb.EOF do
  begin
   mini3 := sltb.FieldAsInteger(sltb.FieldIndex['collect']) * 3;
   sltb.Next;
  end;
 label15.Caption := IntToStr(mini1)+'+ 3 x '+IntToStr(mini3 div 3)+' = '+IntToStr(mini1 + mini3); 
//sdsad
 edit1.Text := level.name;
 SpinEdit1.Value := level.angrylife;
 ComboBox1.ItemIndex := level.leveltype;
 resetMission;
 loadData;
end;

procedure TfrmMissions.Button1Click(Sender: TObject);
begin
  sldb.ExecSQL(Format('UPDATE Levels SET name = "%s", angrylife = %d, leveltype = %d WHERE level_id = %d',
                      [Edit1.Text, SpinEdit1.Value, ComboBox1.ItemIndex, level.id]));
  level.name := Edit1.Text;
  level.angrylife := SpinEdit1.Value;
  level.leveltype := ComboBox1.ItemIndex;

  frmLevel.level := level;
  frmLevel.SpinEdit2.Value := level.angrylife;
  frmLevel.ComboBox1.ItemIndex := level.leveltype;

  ModalResult := mrOk;
end;

procedure TfrmMissions.loadData;
var i : integer;
    sltb: TSQLIteTable;
begin
try
 sltb := sldb.GetTable(Format('SELECT * FROM Missions WHERE level_id = %d ORDER By mission_id',[level.id]));
 i := 0;
 ListBox1.Items.Clear;
 while not sltb.EOF do
  begin
   SetLength(missions , i + 1);
   missions[i].id := sltb.FieldAsInteger(sltb.FieldIndex['mission_id']);
   missions[i].level_id := sltb.FieldAsInteger(sltb.FieldIndex['level_id']);
   missions[i].task := sltb.FieldAsString(sltb.FieldIndex['task']);
   missions[i].status := sltb.FieldAsInteger(sltb.FieldIndex['status']);
   missions[i].mission_type := sltb.FieldAsInteger(sltb.FieldIndex['type']);
   missions[i].cumul := sltb.FieldAsInteger(sltb.FieldIndex['cumulative']) = 1;
   missions[i].value := sltb.FieldAsInteger(sltb.FieldIndex['value']);
   ListBox1.Items.Add(missions[i].task);
   inc(i);
   sltb.Next;
  end;
  ComboBox3.Items.Clear;
 for i := 0 to high(objects) do
  begin
   ComboBox3.Items.Add(objects[i].Name);
  end;
except
end;
end;

procedure TfrmMissions.Button2Click(Sender: TObject);
var sltb: TSQLIteTable;
    i :integer;
begin
 if mission.id > 0 then
  begin
   sldb.ExecSQL(Format('DELETE FROM MissionsRects WHERE mission_id = %d', [mission.id]));
   if ComboBox2.ItemIndex = 2 then
    begin
     sldb.ExecSQL(Format('INSERT INTO MissionsRects(mission_id, x, y, width, height) VALUES (%d, %d, %d, %d, %d);',
           [mission.id, SpinEdit3.Value, SpinEdit4.Value , SpinEdit5.Value , SpinEdit6.Value]));
    end;
   sldb.ExecSQL(Format('UPDATE Missions SET task = "%s", type = %d, cumulative = %d, value = %d WHERE mission_id = %d;',
           [Edit2.text, ComboBox2.ItemIndex, IfThen(CheckBox1.Checked, 1,0), SpinEdit2.Value, mission.id]));
  loadData;
  end
 else
  begin
   sldb.ExecSQL(Format('INSERT INTO Missions(level_id, task, status, type, cumulative, value, progress) VALUES (%d, "%s", %d, %d, %d, %d, %d);',
           [level.id, Edit2.text, 0, ComboBox2.ItemIndex, IfThen(CheckBox1.Checked, 1,0), SpinEdit2.Value, 0]));
   loadData;
   mission.id := missions[ListBox1.Items.Count - 1].id;
   if ComboBox2.ItemIndex = 2 then
    begin
     sldb.ExecSQL(Format('INSERT INTO MissionsRects(mission_id, x, y, width, height) VALUES (%d, %d, %d, %d, %d);',
           [mission.id, SpinEdit3.Value, SpinEdit4.Value , SpinEdit5.Value , SpinEdit6.Value]));
    end;
   if ComboBox2.ItemIndex = 1 then
    begin
     for i := 0 to high(missionObjects) do
      begin
       sldb.ExecSQL(Format('INSERT INTO MissionsXObjects (mission_id, object_id) VALUES(%d, %d)', [mission.id, missionObjects[i].object_id]));
      end;
    end;
  end;
 resetMission;
end;

procedure TfrmMissions.ListBox1DblClick(Sender: TObject);
begin
 if MessageDlg('Doriti sa stergeti obiectivul?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
     sldb.ExecSQL(Format('DELETE FROM Missions WHERE mission_id = %d', [missions[ListBox1.ItemIndex].id]));
    loadData;
    resetMission;
  end;
end;

procedure TfrmMissions.ComboBox2Change(Sender: TObject);
var sltb: TSQLIteTable;
begin
 pRect.Visible := false;
 pObiecte.Visible := false;
 if not missionLoad then
  begin
   sldb.ExecSQL(Format('DELETE FROM MissionsXObjects WHERE mission_id = %d', [mission.id]));
   setlength(missionObjects, 0);
   sldb.ExecSQL(Format('DELETE FROM MissionsRects WHERE mission_id = %d', [mission.id]));
  end;
  missionLoad := false;
 if  (ComboBox2.ItemIndex =  2) then
  begin
    pRect.Visible := true;
    SpinEdit3.Value := 0;
    SpinEdit4.Value := 0;
    SpinEdit5.Value := 0;
    SpinEdit6.Value := 0;
    sltb := sldb.GetTable(Format('SELECT * FROM MissionsRects WHERE mission_id = %d',[mission.id]));
   while not sltb.EOF do
    begin
     SpinEdit3.Value := sltb.FieldAsInteger(sltb.FieldIndex['x']);
     SpinEdit4.Value := sltb.FieldAsInteger(sltb.FieldIndex['y']);
     SpinEdit5.Value := sltb.FieldAsInteger(sltb.FieldIndex['width']);
     SpinEdit6.Value := sltb.FieldAsInteger(sltb.FieldIndex['height']);
     sltb.Next;
    end;
  end;
 if (ComboBox2.ItemIndex = 1) then
  begin
   pObiecte.Visible := true;
   ComboBox3.ItemIndex := -1;
   loadMissionObjects;
  end;
end;

procedure TfrmMissions.ComboBox3Change(Sender: TObject);
begin
Image1.Picture.LoadFromFile(ExtractFilepath(application.exename) + objects[ComboBox3.ItemIndex].MainFrame);
end;

procedure TfrmMissions.Button4Click(Sender: TObject);
var k, i : integer;
    exist : boolean;
begin
  if ComboBox3.ItemIndex < 0 then
   begin
   ShowMessage('Nu ai selectat obiect ( casca ochii :P )!!!');
   end
  else
   begin
    exist := false;
    for i := 0 to high(missionObjects) do
     begin
      if missionObjects[i].object_id = objects[ComboBox3.ItemIndex].id then
       begin
        exist := true;
        break;
       end;
     end;
    if exist then
     begin
      ShowMessage('Obiect existent ( casca ochii :P ) !!!');
     end
    else
     begin
      if mission.id > 0 then
       begin
        sldb.ExecSQL(Format('INSERT INTO MissionsXObjects (mission_id, object_id) VALUES(%d, %d)', [mission.id, objects[ComboBox3.ItemIndex].id]));
       end;
       k := length(missionObjects);
       setlength(missionObjects, k+1);
       missionObjects[k].mission_id := mission.id;
       missionObjects[k].object_id := objects[ComboBox3.ItemIndex].id;
       missionObjects[k].objName := objects[ComboBox3.ItemIndex].Name;
       ListBox2.Items.Add(objects[ComboBox3.ItemIndex].Name);
     end;
   end;
end;

procedure TfrmMissions.loadMission;
var sltb : TSQLIteTable;
begin
 missionLoad := true;
 sltb := sldb.GetTable(Format('SELECT * FROM Missions WHERE mission_id = %d',[missions[ListBox1.ItemIndex].id]));
 while not sltb.EOF do
  begin
    mission.id := missions[ListBox1.ItemIndex].id;
    mission.status := 0;
    mission.mission_type := sltb.FieldAsInteger(sltb.FieldIndex['type']);
    ComboBox2.ItemIndex :=  mission.mission_type;
    mission.value :=  sltb.FieldAsInteger(sltb.FieldIndex['value']);
    SpinEdit2.Value := mission.value;
    mission.cumul :=  sltb.FieldAsInteger(sltb.FieldIndex['cumulative']) = 1;
    CheckBox1.Checked := mission.cumul;
    mission.task := sltb.FieldAsString(sltb.FieldIndex['task']);
    edit2.Text := mission.task;
    sltb.Next;
  end;
  ComboBox2.OnChange( nil );
end;

procedure TfrmMissions.resetMission;
begin
  mission.id := 0;
  mission.level_id := level.id;
  mission.status := 0;
  mission.mission_type := 0;
  mission.value := 0;
  mission.cumul := false;
  mission.task := '';
 Edit2.Text := '';
 pObiecte.Visible := false;
 pRect.Visible := false;
 CheckBox1.Checked := false;
 ComboBox2.ItemIndex := 0;
  spinedit2.Value := 0;
  spinedit3.Value := 0;
  spinedit4.Value := 0;
  spinedit5.Value := 0;
  spinedit6.Value := 0;
  missionLoad := false;
  SetLength(missionObjects, 0);
  
end;

procedure TfrmMissions.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_down then
   begin
     loadMission;
   end;
end;

procedure TfrmMissions.ListBox1Click(Sender: TObject);
begin
 loadMission;
end;

procedure TfrmMissions.Button3Click(Sender: TObject);
begin
 resetMission;
end;

procedure TfrmMissions.ListBox2DblClick(Sender: TObject);
begin
 if MessageDlg('Doriti sa stergeti obiectul?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
     sldb.ExecSQL(Format('DELETE FROM MissionsXObjects WHERE object_id = %d', [missionObjects[ListBox2.ItemIndex].object_id]));
    loadMissionObjects;
  end;
end;

procedure TfrmMissions.loadMissionObjects;
var i : integer;
  sltb: TSQLIteTable;
begin
   sltb := sldb.GetTable(Format('SELECT mo.*, o.name FROM MissionsXObjects mo LEFT JOIN Objects o ON o.object_id = mo.object_id WHERE mo.mission_id = %d',[mission.id]));
   i := 0;
   ListBox2.Items.Clear;
   while not sltb.EOF do
    begin
     SetLength(missionObjects , i + 1);
     missionObjects[i].mission_id := sltb.FieldAsInteger(sltb.FieldIndex['mission_id']);
     missionObjects[i].object_id := sltb.FieldAsInteger(sltb.FieldIndex['object_id']);
     missionObjects[i].objName := sltb.FieldAsString(sltb.FieldIndex['name']);
     ListBox2.Items.Add(missionObjects[i].objName);
     inc(i);
     sltb.Next;
    end;
end;

end.
