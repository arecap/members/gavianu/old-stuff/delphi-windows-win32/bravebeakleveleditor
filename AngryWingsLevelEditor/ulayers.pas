unit ulayers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, entities, SQLite3, SQLiteTable3, pngimage;

type
  TfrmLayers = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    lbLayers: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    cbType: TComboBox;
    Label3: TLabel;
    CheckBox1: TCheckBox;
    img: TImage;
    Button1: TButton;
    Button2: TButton;
    OpenDialog1: TOpenDialog;
    eName: TEdit;
    procedure FormShow(Sender: TObject);
    procedure imgClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure lbLayersClick(Sender: TObject);
    procedure lbLayersDblClick(Sender: TObject);
    procedure lbLayersKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    layers : TGameLayers;
    sldb: TSQLiteDatabase;
    objIdx : integer;
    image : String;

    procedure loadData;
    procedure loadObject;
  end;

var
  frmLayers: TfrmLayers;

implementation

uses StrUtils;
{$R *.dfm}

{ TForm1 }

procedure TfrmLayers.loadData;
var i : integer;
    sltb: TSQLIteTable;
    slDBPath : string;
begin
eName.Text := '';
eName.SetFocus;
cbType.ItemIndex := 0;
img.Picture := nil;
image := '';
objIdx := -1;

slDBPath := ExtractFilepath(application.exename)
+ 'angry.sqlite';
sldb := TSQLiteDatabase.Create(slDBPath);

try
 sltb := sldb.GetTable('SELECT * FROM Layers');
 i := 0;
 lbLayers.Items.Clear;
 while not sltb.EOF do
  begin
   SetLength(layers , i + 1);
   layers[i].id := sltb.FieldAsInteger(sltb.FieldIndex['layer_id']);
   layers[i].Name := sltb.FieldAsString(sltb.FieldIndex['Name']);
   layers[i].layerType := sltb.FieldAsInteger(sltb.FieldIndex['type']);
   layers[i].Image := sltb.FieldAsString(sltb.FieldIndex['main_frame']);
   lbLayers.Items.Add(layers[i].Name);
   inc(i);
   sltb.Next;
  end;
except
end;
end;

procedure TfrmLayers.loadObject;
begin
eName.Text := layers[objIdx].Name;
cbType.ItemIndex := layers[objIdx].layerType;
image :=   layers[objIdx].Image;
img.Picture.LoadFromFile(ExtractFilepath(application.exename) + image);
end;

procedure TfrmLayers.FormShow(Sender: TObject);
begin
  loadData;
end;

procedure TfrmLayers.imgClick(Sender: TObject);
var FromF, ToF: file;
  NumRead, NumWritten: Integer;
  Buf: array[1..2048] of Char;
begin
  if OpenDialog1.Execute then
   begin
     AssignFile(FromF, OpenDialog1.FileName);
     Reset(FromF, 1);
     image :=  AnsiRightStr(openDialog1.FileName, AnsiPos('\', AnsiReverseString(openDialog1.FileName)) - 1);
     AssignFile(ToF, ExtractFilepath(application.exename)+image);
     Rewrite (ToF, 1);
      repeat
        BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
        BlockWrite(ToF, Buf, NumRead, NumWritten);
      until (NumRead = 0) or (NumWritten <> NumRead);
      CloseFile(FromF);
      CloseFile(ToF);
      img.Picture.LoadFromFile(ExtractFilepath(application.exename)+image);
   end;
end;

procedure TfrmLayers.Button1Click(Sender: TObject);
begin
eName.Text := '';
img.Picture := nil;
cbType.ItemIndex := 0;
image := '';
objIdx := -1;
end;

procedure TfrmLayers.Button2Click(Sender: TObject);
var strSQL : String;
begin

 if objIdx = -1 then
  begin
    strSQL := Format('INSERT INTO Layers(main_frame, name, type) VALUES ("%s", "%s", %d);',
           [image, eName.Text, cbType.ItemIndex]);
  end
 else
  begin
    strSQL := Format('UPDATE Layers SET'+
                        ' Name = "%s", type = %d, main_frame = "%s"' +
                        ' WHERE layer_id = %d;',
           [eName.Text, cbType.ItemIndex, image, layers[objIdx].id]);
  end;
 sldb.ExecSQL(strSQL);
 loadData;
end;

procedure TfrmLayers.lbLayersClick(Sender: TObject);
begin
 objIdx := lbLayers.ItemIndex;
 loadObject;
end;

procedure TfrmLayers.lbLayersDblClick(Sender: TObject);
begin
 if MessageDlg('Doriti sa stergeti layerul?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
     sldb.ExecSQL(Format('DELETE FROM Layers WHERE layer_id = %d', [layers[lbLayers.ItemIndex].id]));
    loadData;
  end;
end;

procedure TfrmLayers.lbLayersKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_down then
   begin
     objIdx := lbLayers.ItemIndex;
     loadObject;
   end;
end;

procedure TfrmLayers.CheckBox1Click(Sender: TObject);
begin
 img.Stretch := CheckBox1.Checked;
end;

end.
